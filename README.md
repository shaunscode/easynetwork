# README #

### Java UDP Network Library ###

This network library is intended to be a plug and play solution to client/server based applications. It was created with games in mind but can easily be adopted to any client/server based network applications, such as the simple chat room application that can be found within the library source.

### Initial goal ###

The initial goal statements are below. I think at this stage it has met at least most of them. 

* The idea is to have a abstract Client/Server classes with an abtract process() function will be handed 'Commands' list, each with arguments, definable by the user.
* The user instanciates a new Command, gives it a type, add any arguments, and sends.
* The framework side of things will handle running a server and listening for connections, handling the connection, and sending commands to single, multiple or all clients.
* The frameworks client side will handle creating the connection, verifying an unique id, and be ready to send and new commands, and have a list of new commands recieved from the server waiting to be processed.
* The framework should also handle connection issues, like timeouts, disconnects, reconnects and network update rate. 
* This would hopefully provide a foundation for a user to talk to a server with easy simply by sending and recieving commands 

### Features ###

With the statements above, things have managed to change and evolve into a different but similar process.

* Simple conneciton creation between Server and Client using ServerNetwork.listen() and ClientNetwork.connect() methods alone. 
* Client management including unique Id's, disconnection and timeout handling
* Simple Packet creation for sending information by extending Packet class, adding fields and sending it off.
* Mutlithreaded networking to handle UDP datagram socket receive and send methods independantly
* Custom serialization of primitives and objects including recursive packing and unpacking using Java Reflection
* Reliable/unreliable packet sending, acknowledgement and resending of dropped reliable packets

### Dependancies ###

This has been written in Java8 alone without the use of any other libraries.

### Getting started ###

The quickest, bare bones way of starting use with this library is through two essential classes:

1. ServerNetwork
2. ClientNetwork

By instantiating the ServerNetwork and ClientNetwork classes within a users application, it's as simple as using the ServerNetwork.listen(<port number>) and the ClientNetwork.connect(<Addess>, <PortNumber>) methods to make a connection.

The ServerNetwork will open a UDP Datagram Socket on the defined port number and listen for incoming Connection Requests. Once receieved, the Server will build a NetworkProfile for the client, send a response and allow the client to continue to send other Packet derived class objects.

### How it really works ###

As above, a users application will need to instantiate the ServerNetwork and ClientNetwork classes within their approriate applications.

Once connected the server and client will send a Ping packet every five seconds which helps determine if either side has timed out should one application shutdown abruptly. This otherwise forms the basis of the client/server connection. More clients can continue to connect to the server. As this library is used only to send/received packets there is no set limit on the number of clients the server will accept. This is upto the user to handle.

To start sending packets, the use must extend the Packet class and implent the public abstract byte getType () method present in the Packet class. This only needs to return an unique number between 10 and 127 to use as an identification number for the Packet. This number must be unique to all other derived Packet classes as it will be preappended to the packet data to identify the Packet sent across the network. The user must register the class with both the ClientNetwork and ServerNetwork class objects using the registerPacketClass() method. There are checks in place to ensure the user registering a duplicate Id.

Users then add class variables to their new Packet derived class for the information to be sent. The fields to be sent must be public in order for the Java Reflectoin to pick them up. Private fields will be ignored. Note: Methods will also be ignored, as it is intended to send structured data for efficency and not serialised objects to be recreated with their methods also intact.

Once ready, ClientNetwork can use the send(<Packet>) or sendReliable(<Packet>) methods to forward their packet to the server.

The ServerNetwork can use the sendPacket(<Packet>, <ClientId>), sendReliable(<Packet>, <ClientId>, broadcast(<Packet>) and broadcastReliable(<Packet>) methods to send packets to one specific or all connected clients.

When finished, the ClientNetwork can use it's own .disconnect() method to send a disconnection message to the server and stop the network thread from continuing to run. For the ServerNetwork, it provides a .disconnect(<ClientId>) method for removing clients, and a .shutdown() method send all clients a disconnection message and stop it's own network thread from continuing to run.

### Packet handling ###

So a packet has been sent, how is it handled? This works the same for both client and server.

By default incoming packets will be stored in a queue within the ClientNetwork/ServerNetwork. The user will have to use the network classes .readPacket() method to retrieve a packet. This method will return null when there are no packets. This is suggested to be use in a while loop to read all packets and handle them until null is retrieved somewhere in the user applications main loop. 

On the other hand, a PacketListener derived class which implements the public void handlePacket (<Packet>) method can be supplied to the ClientNetwork or ServerNetwork constructor, in which case each packet received will be handed to this method.

The two methods above for handling the Packets will receive the packet object as a Packet type object. It's expected the user will will handle the casting of this object to the appropriate object for use. 

### Serialization ###

The custom serializer used will not serialize a complete class object as the default java serialization would. Therefore what can be serialized is somewhat limited, though within reason for the goal of simple, fast

This serializer uses Java Reflection to write and read the Packet derived objects at each end. It's a custom serializer which reads the class files Fields to extract the data to send, reads the Fields values and stores them in a byte array using a ByteBuffer. Each Packet derived class's identifying byte type is preappended to the byte array and packed into a DatagramPacket for sending.

On arrivale, the DatagramPacket byte data is retrieved, the first byte is read by a ByteBuffer and determines which packet type has been sent. Using Java Reflection again, a new object of that type is instantiated, the class types Fields are retrieved iterated over to populate the new objects fields in the same order they were read.

For this to work, each class object the user wishes to be able to send must extends the Packet class and implement a getType() method, which simple returns a byte value intended to use as a Packet Identifier when sending and receiving the packet. Each field to be sent must be Public as the getFields() method of the Class object used for Reflection will not pickup Private/Protected fields.

As stated serialization is not 100% complete for all classes and types.

1. All primitive and primitive class types are support
2. Strings are supported, with a max char length imposed
3. Objects of primitive types are handled recursively to allow objects within objects.

Arrays have not be implemented. This is a future goal once I figure out how arrays and reflection work. 

The mention of objects above is more so class objects of primitive fields similar to a C/C++ structure. Objects as fields is possible as the serializer recursively assumes a field to be an object if it is not a primitive

### Efficiency ###

This is only a brief experiment, but I wanted to find out how much more efficient this style of serializing was compared to Java's default Serialization. 

First I built a simple class extending Packet, as a user would, and added a number of fields with dummy values to measure

import com.network.packets.Packet;
public class TestPacket extends Packet {
	public boolean b = true;
	public short sh = 1;
	public char c = 'c';
	public int x = 12;
	public long l = 12300;
	public float f = 123.012f;
	public double d = 123123123;
	public Boolean B = true;
	public Short Sh = 1;
	public Character C = 'c';
	public Integer X = 12;
	public Long L = 12300l;
	public Float F = 123.012f;
	public Double D = 123123123d;
	@Override
	public byte getType() {	
		return 123;
	}
}

Then following the code supplied here:
https://stackoverflow.com/questions/3938122/how-to-get-amount-of-serialized-bytes-representing-a-java-object
and the libraries own custom Serializer.getObjectByteSize() which is used to determine the byte array size to store the serialized data and pack into the DatagramPacket for sending, to compare the difference:

* Java Serialization: 817 bytes
* Custom Serialization: 179 bytes

That's 21.9% the size! 

Unforunately, adding a single String field in there of "TestPacket" brings that the custom sizes upto 857/307, which is only 35% in size. This is due to the use of a Char buffer of 128 bytes (by default) to send the strings instead of calculating the String length and adjusting buffer size on the fly. This is a future goal and would ensure only 1 byte per char is used to send character data. 

It took an addition five strings (six total) for the sizes to equal out at 943/947 bytes, with the custom serializer wasting (6 * 128) - (6 * 11) = 702 bytes of empty space due to the char buffer currently in use. 

### Conclusion ###

This started out as a personal project to learn more about networking, discovering reflection along the way and has turned into something I think people could also use.

I still have a few more features I would like to add:
* Implementing arrays into the serializer
* Proper logging of messages
* Proper exception handling with meaningful messages, so I can;
* Remove all System.out.print() calls to display messages, and can;
* Allow custom exception handler injection to consume messages and output to user destination

Long term goals are to change the Packet extention and sending Packets one by one to a Message extention which, when sending, are placed in an outgoing queue for packeting into a Packet which then gets sent. This would allow maximum packet packing and reduce multiple packet overhead of sending essentially the same header over and over. 

