package com.network.utils;

public class BitQueue {

	
	private int writeByteIndex = 0;	
	private byte writeBitIndex = 0;	

	private int readByteIndex = 0;
	private byte readBitIndex = 0;
	
	private byte[] BITS = {-128, 64, 32, 16, 8, 4, 2, 1};
	private byte[] byteArr;
	
	public BitQueue (int inSize) {
		
		int numTotalBytes = inSize / Byte.SIZE;
		int mod = inSize % Byte.SIZE;
		int arrLen = numTotalBytes + (mod > 0? 1: 0);
		
		byteArr = new byte[arrLen];		
	}
	
	public void setQueue (byte[] inByteArr) {
		reset();
		byteArr = inByteArr;
	}

	public void reset () {
		writeByteIndex = 0;	
		writeBitIndex = 0;	

		readByteIndex = 0;
		readBitIndex = 0;		
	}
	
	public void clear () {
		for (int i = 0; i < byteArr.length; i++) {
			byteArr[i] = 0;
		}
		reset();
	}
	
	public BitQueue (byte[] inArr) {
		byteArr = inArr;
	}
	
	public int getSize () {	
		return byteArr.length * Byte.SIZE;
	}
	
	public void add (String inValue) {
		char[] chars = inValue.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == '1') {
				add(true);
			} else {
				add(false);
			}
			
		}
	}
	
	public void add (boolean... inValues) {
		for (int i = 0; i < inValues.length; i++) {
			try {
				if (inValues[i]) {
					byteArr[writeByteIndex] |= BITS[writeBitIndex];
				}
				
				writeBitIndex++;
				if (writeBitIndex == 8) {
					writeByteIndex++;
					writeBitIndex = 0;
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				throw new ArrayIndexOutOfBoundsException("Attemped to set bit index beyond backing byte array size");
			}
		}
	}
	
	public void add (boolean inValue) {

	}
		
	public boolean hasNext() {
		//lower byte? of course i has a next
		if (readByteIndex < writeByteIndex) {
			return true;
		} 
		
		//equal byte? might have a next, check bit level
		if (readByteIndex == writeByteIndex) {
			if (readBitIndex < writeBitIndex) {
				//write index sits on the next index to write to (so its empty)
				//read index ticks over to this empty index in hasNext()'s last iteration
				return true;
			}
		}
			
		//must've read the write bit index, so no, doesn't have a next
		return false;
		
	}
	
	
	public boolean next () {
		if (readByteIndex >= byteArr.length) {
			throw new ArrayIndexOutOfBoundsException("readByteIndex of " + readByteIndex + " out of bounds (byteArr.lenght: " + byteArr.length + ")");
		}
		boolean value = (byteArr[readByteIndex] & BITS[readBitIndex]) == 0? false: true;
		readBitIndex++;
		if (readBitIndex == Byte.SIZE) {
			readByteIndex++;
			readBitIndex = 0;
		}
		return value;
	}
	
	public void setReadIndex (int inIndex) {
		readByteIndex = inIndex / Byte.SIZE;
		readBitIndex = (byte) (inIndex % Byte.SIZE);
	}
	
	public int getReadIndex () {
		return (readByteIndex * Byte.SIZE) + readBitIndex; 
	}
	
	public void setWriteIndex (int inIndex) {
		writeByteIndex = inIndex / Byte.SIZE;
		writeBitIndex = (byte) (inIndex % Byte.SIZE);
	}
	
	public int getWriteIndex () {
		return (writeByteIndex * Byte.SIZE) + writeBitIndex; 
	}
	
	public boolean getIndex (int inIndex) {
		int byteIndex = inIndex / Byte.SIZE;
		byte bitIndex = (byte) (inIndex % Byte.SIZE);
		
		return (byteArr[byteIndex] & BITS[bitIndex]) == 0? false: true;
	}
	
	public void setIndex (int inIndex) {
		setIndex(inIndex, true);
	}
	
	
	
	public void setIndex (int inIndex, boolean inValue) {
		int byteIndex = inIndex / Byte.SIZE;
		byte bitIndex = (byte) (inIndex % Byte.SIZE);

		if (inValue) {
			byteArr[byteIndex] |= BITS[bitIndex];
		} else {
			byteArr[byteIndex] |= 0;
		}
	}
	
	/*
	 * Returns the backing byte array 
	 *  @return the backing byte array
	 */
	public byte[] toByteArray () {
		return byteArr;
	}
	
	public String toString () {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < byteArr.length; i++) {
			sb.append(String.format("%8s", Integer.toBinaryString(byteArr[i] & 0xFF)).replace(' ', '0'));
			if (i != byteArr.length) {
				sb.append(",");
			}
		}
		
		return sb.toString();
	}
	
}
