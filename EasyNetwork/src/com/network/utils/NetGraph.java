package com.network.utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.network.profiles.NetworkProfile;

public class NetGraph extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private NetGraphPanel graphsPanel = null;
	
	private Color emptyColor = Color.lightGray;
	private Color sentColor = new Color(0, 255, 50);
	private Color droppedColor = new Color(255, 20, 20);
	private Color reliableColor = new Color(255, 216, 0);
	private Color resentColor = new Color(255, 120, 30);
	private Color currentColor = new Color(100, 200, 255);
	private Color outlineColor = Color.gray;
	
	public NetGraph (String inTitle, HashMap<Integer, NetworkProfile> inProfiles) {
		
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		
		this.graphsPanel = new NetGraphPanel(inProfiles);
		this.getContentPane().add(graphsPanel);
		
		this.setTitle(inTitle);
		this.setSize(800, 300);
		this.setLocation(10, 10);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.pack();
		this.setVisible(true);
		
	}
	
	public void repaint () {
		super.repaint();
		this.revalidate();
		this.pack();
	}
		
	private class NetGraphPanel extends JPanel {
		
		private static final long serialVersionUID = 1L;

		private HashMap<Integer, NetworkProfile> profiles = new HashMap<Integer, NetworkProfile>();
		private HashMap<Integer, ProfileNetgraph> graphs = new HashMap<Integer, ProfileNetgraph>();

		private Label numProfilesLabel = new Label();
		
		public NetGraphPanel (HashMap<Integer, NetworkProfile> inProfiles) {
			
			this.setLayout(new GridBagLayout());			
			this.profiles = inProfiles;
			
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.anchor = GridBagConstraints.LINE_START;
			this.add(numProfilesLabel);
		}
		
		private void addAndRemoveGraphs () {
			for (Integer profileId: profiles.keySet()) {
				if (!graphs.containsKey(profileId)) {
					addGraph(profiles.get(profileId));
					this.revalidate();
				} 
			}
			for (Integer graphId: graphs.keySet()) {
				if (!profiles.containsKey(graphId)) {
					removeGraph(graphId);
				} 
			}
		
		}
		
		private ProfileNetgraph addGraph (NetworkProfile inProfile) {
			ProfileNetgraph graph = new ProfileNetgraph(inProfile);
			graphs.put(inProfile.profileId, graph);
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = graphs.size() + 1;
			this.add(graph, c);
			return graph;
		}
		
		private void removeGraph (int inGraphId) {
			ProfileNetgraph graph = graphs.remove(inGraphId);
			this.remove(graph);
		}
		
		@Override 
		public void paintComponent (Graphics g) { 
			addAndRemoveGraphs();
			numProfilesLabel.setText("Profiles connected: " + profiles.size());
			
		}
		
	}
	
	private class ProfileNetgraph extends JPanel {

		private static final long serialVersionUID = 1L;
		private NetworkProfile profile;
		private Label msgsOutSize = new Label();
		private PacketsReceivedGraph packetsRecieved;
		private PacketsArrivedGraph packetsArrived;
		private PacketsSentGraph packetsSent;
		
		public ProfileNetgraph (NetworkProfile inProfile) {
			
			this.profile = inProfile;
			
			this.packetsRecieved = new PacketsReceivedGraph(profile);
			this.packetsSent = new PacketsSentGraph(profile);
			this.packetsArrived = new PacketsArrivedGraph(profile);
			
			this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			this.setLayout(new GridBagLayout());
			
			GridBagConstraints c = new GridBagConstraints ();
			
			c.gridx = 0;
			c.gridy = 0;
			//c.fill = GridBagConstraints.NONE;
			this.add(new Label("Profile"), c);
			
			c.gridx = 0;
			c.gridy = 1;
			//c.fill = GridBagConstraints.NONE;
			this.add(new Label("" + this.profile.profileId), c);
						
			c.gridx = 1;
			c.gridy = 0;
			//c.fill = GridBagConstraints.NONE;
			this.add(new Label("Sent"), c);
			
			c.gridx = 2;
			c.gridy = 0;
			//c.fill = GridBagConstraints.HORIZONTAL;
			this.add(packetsSent);

			c.gridx = 1;
			c.gridy = 1;
			//c.fill = GridBagConstraints.NONE;
			this.add(new Label("Arrived"), c);

			c.gridx = 2;
			c.gridy = 1;
			//c.fill = GridBagConstraints.HORIZONTAL;
			this.add(packetsArrived, c);
			
			c.gridx = 1;
			c.gridy = 2;
			//c.fill = GridBagConstraints.NONE;
			this.add(new Label("Received"), c);

			c.gridx = 2;
			c.gridy = 2;
			//c.fill = GridBagConstraints.HORIZONTAL;
			this.add(packetsRecieved, c);
			
		}
		
		
		
	}
	
	private class PacketsGraph extends JPanel {
		private static final long serialVersionUID = 1L;
		protected NetworkProfile profile;
		protected int barWidth = 3;
		protected int barHeight = 25;
		
		public PacketsGraph (NetworkProfile inProfile) {
			this.profile = inProfile;
			this.setPreferredSize(new Dimension(profile.outPacketsBuffer.length * barWidth, barHeight));
		}
		
	}
	
	private class PacketsSentGraph extends PacketsGraph {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public PacketsSentGraph(NetworkProfile inProfile) {
			super(inProfile);
		}

		@Override 
		public void paintComponent (Graphics g) { 
			
			for (int i = 0; i < profile.outPacketsBuffer.length; i++) {
				if (profile.outPacketsBuffer[i] == null) {
					g.setColor(emptyColor);
				} else if (profile.outPacketsBuffer[i].resent) {
					g.setColor(resentColor);
				} else if (profile.outPacketsBuffer[i].hasReliableMessages) {
					g.setColor(reliableColor);					
				} else {
					g.setColor(sentColor);
				}
				
				g.fillRect(i * barWidth, 0, i * barWidth + barWidth, barHeight);
				g.setColor(emptyColor);
				g.drawRect(i * barWidth, 0, i * barWidth + barWidth, barHeight);
			}
			
			g.setColor(currentColor);
			int index = NetUtils.getBufferIndex((int)profile.packetOutId);
			g.fillRect(index * barWidth, 0, barWidth, barHeight);
			g.setColor(outlineColor);
			g.drawRect(index * barWidth, 0, barWidth, barHeight);
			
		
		}	
	}
	
	

	private class PacketsArrivedGraph extends PacketsGraph {

		private static final long serialVersionUID = 1L;
		
		public PacketsArrivedGraph(NetworkProfile inProfile) {
			super(inProfile);
		}
		
		@Override 
		public void paintComponent (Graphics g) { 
			
			for (int i = 0; i < profile.outPacketsBuffer.length; i++) {
				if (profile.outPacketsBuffer[i] != null && profile.outPacketsBuffer[i].wasReceived) {
					g.setColor(sentColor);
				} else {
					g.setColor(droppedColor);
				}
				
				g.fillRect(i * barWidth, 0, (i * barWidth) + barWidth, barHeight);
				g.setColor(emptyColor);
				g.drawRect(i * barWidth, 0, (i * barWidth) + barWidth, barHeight);
			}
			
			g.setColor(currentColor);
			int index = NetUtils.getBufferIndex((int)profile.packetOutId);
			g.fillRect(index * barWidth, 0, barWidth, barHeight);
			g.setColor(outlineColor);
			g.drawRect(index * barWidth, 0, barWidth, barHeight);
			
		}	
	}

	
	
	
	
	private class PacketsReceivedGraph extends PacketsGraph {

		private static final long serialVersionUID = 1L;
		
		public PacketsReceivedGraph(NetworkProfile inProfile) {
			super(inProfile);
		}
		
		@Override 
		public void paintComponent (Graphics g) { 
			
			for (int i = 0; i < profile.inPacketsRegister.length; i++) {
				if (profile.inPacketsRegister[i]) {
					g.setColor(sentColor);
				} else {
					g.setColor(droppedColor);
				}
				
				g.fillRect(i * barWidth, 0, (i * barWidth) + barWidth, barHeight);
				g.setColor(emptyColor);
				g.drawRect(i * barWidth, 0, (i * barWidth) + barWidth, barHeight);
			}
			
			g.setColor(Color.cyan);
			int index = NetUtils.getBufferIndex((int)profile.packetInId);
			g.fillRect(index * barWidth, 0, barWidth, barHeight);
			g.setColor(emptyColor);
			g.drawRect(index * barWidth, 0, barWidth, barHeight);
			
		}	
	}

	
}
