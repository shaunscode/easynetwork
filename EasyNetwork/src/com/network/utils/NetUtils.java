package com.network.utils;

import java.lang.reflect.Field;

import com.network.common.Constants;
import com.network.packing.Message;

public class NetUtils {
	public static float getNanoAsMilli (long inNano) {
		return (inNano / 1000000.00f);
	}
	
	public static String getBinary (int inNumber) {
		return String.format("%32s", Integer.toBinaryString(inNumber)).replace(" ", "0");
	}
	
	public static boolean isPacketBufferIndexGreaterThan (int inIndex, int inToIndex) {
		
		return ( ( inIndex > inToIndex ) && ( inIndex - inToIndex <= 32768 ) ) ||    
	               ( ( inIndex < inToIndex ) && ( inToIndex - inIndex  > 32768 ) );
	 
	}
	
	public static int getBufferIndex (int inPacketId) {
		return (inPacketId % Constants.PROFILE_SENT_PACKETS_BUFFER_LENGTH);
	}
	
	public static int nextBufferIndex (int inIndex) {
		return (inIndex + 1 >= Constants.PROFILE_SENT_PACKETS_BUFFER_LENGTH? 0: ++inIndex);
	}
	
	public static int prevBufferIndex (int inIndex) {
		return (inIndex - 1 < 0? Constants.PROFILE_SENT_PACKETS_BUFFER_LENGTH - 1: --inIndex);
	}
	
	public static int clampBufferIndex (int inIndex) {
		
		while (inIndex < 0) {
			inIndex += Constants.PROFILE_SENT_PACKETS_BUFFER_LENGTH;
		}
		while (inIndex >= Constants.PROFILE_SENT_PACKETS_BUFFER_LENGTH) {
			inIndex -= Constants.PROFILE_SENT_PACKETS_BUFFER_LENGTH;
		}
		return inIndex;
	}
	
	
}
