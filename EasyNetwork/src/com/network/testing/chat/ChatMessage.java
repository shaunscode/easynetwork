package com.network.testing.chat;
import com.network.packing.Message;

public class ChatMessage extends Message {
	
	public String message = "";
	public byte[] bigData = new byte[1400];
	
	public ChatMessage () {
		for (int i = 0; i < 1400; i++) {
			bigData[i] = (byte)(Math.random() > 0.5? 1: 0);
		}
	}
	
	public String toString () {
		return "Msg: " + message;
	}
	
}

