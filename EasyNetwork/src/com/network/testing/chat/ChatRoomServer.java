package com.network.testing.chat;

import java.util.List;

import com.network.ServerNetwork;
import com.network.exceptions.NetRunningMsgReadException;
import com.network.exceptions.NetStartupException;
import com.network.packing.Message;

//ChatROomServer
//This is an example oh basic (VERY basic) use of the ServerNetwork obj for the library.
public class ChatRoomServer {
	
	//Create a ServerNetwork object for network communications
	//Here, we are passing in an anonymous PacketListener object, implementing the handlePacket method
	private ServerNetwork net = new ServerNetwork();
	
	//Constructor
	public ChatRoomServer () throws NetStartupException, InterruptedException, NetRunningMsgReadException {

		//We need to register the custom packet, ChatMessage, with the network. 
		//We do this by passing in the class object of the packet.
		net.registerClass(ChatMessage.class);
		
		//Immediately we can use the ServerNetwork.listen() method, providing a port to listen on, to wait for incoming packets.
		net.listen(45000);
	
					
		
		while (net.isListening()) {
			
			List<Message> msgs = net.readMessages();
			
			 for (Message msg: msgs) {
				 ChatMessage chatMsg = (ChatMessage)msg;
				 handleMessage(chatMsg);
			 }
			 Thread.sleep(1000);
		}
		
	}//end ChatRoomServer application.
	
	
	public void handleMessage(ChatMessage inMsg) {
		
		//A ChatMessage packet is the only type of custom packet created for use with this application
		//so we can easily cast it without the need to use instanceof or check what packet type it is
		ChatMessage c = new ChatMessage();
		c.message = "User " + inMsg.msgSenderId + " says: " + inMsg.message;
		
		//User feedback to show the message received by the server
		System.out.println("Boardcasting: " + c.message);
		
		//Using the ServerNetwork.broadcast() method, the ChatMessage will be redirected andsent to 
		//all connected Clients
		net.writeBroadcastMessage(c);
		net.flushMessages();
		
	}
	
	
	//Main method to run ChatRoomServer application
	public static void main (String[] args) throws NetStartupException, InterruptedException, NetRunningMsgReadException {		
		
		//Instantiate ChatRoomServer object
		new ChatRoomServer();		
	}
	
}
