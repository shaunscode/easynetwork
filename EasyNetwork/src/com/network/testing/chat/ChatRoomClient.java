package com.network.testing.chat;
import java.util.List;

import com.network.ClientNetwork;
import com.network.connection.ConnectionState;
import com.network.exceptions.NetRunningMsgReadException;
import com.network.exceptions.NetRunningSerializationException;
import com.network.exceptions.NetStartupException;
import com.network.interfaces.MessageListener;
import com.network.packing.Message;

//ChatRoomClient
//This is an example oh basic (VERY basic) use of the ClientNetwork obj for the library.
public class ChatRoomClient {


	//Create a ClientNetwork object
	//Here, we are passing in an anonymous PacketListener object, implementing the handlePacket method 
	private ClientNetwork net = new ClientNetwork();	
	
	//Constructor
	public ChatRoomClient () throws NetStartupException, InterruptedException, NetRunningMsgReadException {
		
		//We need to register the custom packet, ChatMessage, with the network. 
		//We do this by passing in the class object of the packet.
		net.registerClass(ChatMessage.class);
		
		//All set, we use the connect() method, passing in the address and port to conect to.
		net.connect("localhost", 45000);
		
		new Thread (new Runnable() {

			@Override
			public void run() {
				while (net.isConnected()) {
					//We prompt the user for some input, which will block until received
					System.out.println("Input > ");
					String str = TextIO.getln();
					
					//Once input is received we can instantiate a new ChatMessage packet to be sent
					ChatMessage msg = new ChatMessage();

					//populate the chat messages message field we defined with the input string
					msg.message = str;
					
					//and use the ClientNetwork.send() method, passing in the ChatMessage packet to be sent.t
					//net.sendMessage(msg);
					
					net.writeAndFlushMessage(msg);
				}
			}
			
		}).start();
		
		
		
		//Once connected we begin running the main loop of the program
		while (net.isConnected()) {
			
			List<Message> msgs = net.readMessages();
			
			 for (Message msg: msgs) {
				 handleMessage((ChatMessage)msg);				 
			 }
			 
			 Thread.sleep(1000);
		}
	
	}//end of ChatRoomClient application

	public void handleMessage(Message inMsg) {
		//A ChatMessage message is the only type of custom packet created for use with this application
		//so we can easily cast it without the need to use instanceof or check what packet type it is
		ChatMessage msg = (ChatMessage)inMsg;
		
		//When receiving a ChatMessage, we're just going to print out the contents to the system console.
		System.out.println(msg.message);
	}
	
	//Starting method to runthe Chatroom Client 
	public static void main (String[] args) throws NetStartupException, InterruptedException, NetRunningMsgReadException {
		
		//Instantiate a new ChatRoomClient obj
		new ChatRoomClient();
	}
	
	
}
