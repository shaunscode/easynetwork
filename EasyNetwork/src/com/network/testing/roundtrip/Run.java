package com.network.testing.roundtrip;

import java.util.List;

import com.network.ClientNetwork;
import com.network.ServerNetwork;
import com.network.exceptions.NetStartupException;
import com.network.packing.Message;
import com.network.testing.chat.ChatMessage;

public class Run {

	public static void main(String[] args) throws NetStartupException, InterruptedException {
		new Run();
	}

	public Run () throws InterruptedException, NetStartupException {
		
		ClientNetwork cnet = new ClientNetwork();
		ServerNetwork snet = new ServerNetwork();
		
		snet.listen(45000);
		cnet.connect("localhost", 45000);
					
		
		while (snet.isListening() && cnet.isConnected()) {
			
			
			
			
			
			
			List<Message> smsgs = snet.readMessages();
			List<Message> cmsgs = cnet.readMessages();
			
			 Thread.sleep(1000);
		}
		
	}
	
}
