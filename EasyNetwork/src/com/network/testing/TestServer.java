package com.network.testing;

import java.io.IOException;
import java.util.Iterator;

import com.network.ServerNetwork;
import com.network.exceptions.NetStartupException;
import com.network.interfaces.MessageListener;
import com.network.packing.Message;
import com.network.profiles.NetworkProfile;

public class TestServer {
	
	public static void main (String[] args) throws IOException, NetStartupException, InterruptedException {
		ServerNetwork net = new ServerNetwork();
	
		net.listen(55556);		

		while (net.isListening()) {
			Thread.sleep(1000);
			System.out.print(".");
		}
		
	}
}
