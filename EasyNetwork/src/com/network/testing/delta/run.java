package com.network.testing.delta;

import com.network.exceptions.NetRunningSerializationException;
import com.network.serialization.Serializer;


public class run {

	
	
	public static void main(String[] args) throws NetRunningSerializationException {

		Serializer s = Serializer.getInstance();
		s.registerClass(TestDeltaMsg.class);
		
		TestDeltaMsg msg = new TestDeltaMsg();
		
		byte[] a = s.deltaSerializeMsg(msg);
		System.out.print("Packed: ");
		if (a.length == 0) {
			System.out.println("nothing packed");
		} else {
			for (int i = 0; i < a.length; i++) {
				System.out.print(a[i] + ", ");
			}
		}
			
		
		msg = new TestDeltaMsg();
		msg.ia = 5;
		
		byte[] b = s.deltaSerializeMsg(msg);
		System.out.print("Packed: ");
		if (b.length == 0) {
			System.out.println("nothing packed");
		} else {
			for (int i = 0; i < b.length; i++) {			
				System.out.print(b[i] + ", ");
			}
		}
		
		
	}

}
