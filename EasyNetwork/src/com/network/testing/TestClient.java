package com.network.testing;

import java.io.IOException;

import com.network.ClientNetwork;
import com.network.exceptions.NetStartupException;
import com.network.interfaces.MessageListener;
import com.network.packing.Message;

public class TestClient {

	public static void main (String[] args) throws IOException, IllegalArgumentException, IllegalAccessException, InterruptedException, NetStartupException {

		ClientNetwork net = new ClientNetwork();

		net.connect("localhost", 55556);
		
		while (net.isConnected()) {
			Thread.sleep(1000);
			System.out.print(".");
		}
	}
}
