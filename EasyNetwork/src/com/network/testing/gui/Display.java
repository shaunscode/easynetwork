package com.network.testing.gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Display extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Ball[] balls = null;
	
	public Display (Ball[] inBalls) {
		this.balls = inBalls;
	}
	
	
	@Override
	public void paintComponent (Graphics g) {
		
		g.setColor(Color.WHITE);
		g.fillRect(0,  0,  Common.SCREEN_WIDTH, Common.SCREEN_HEIGHT);

		int halfDia = Common.BALL_DIA / 2;
		for (Ball ball: balls) {
			//g.setColor(Color.LIGHT_GRAY);
			//g.drawLine((int)(Constants.SCREEN_WIDTH / 2), (int)(Constants.SCREEN_HEIGHT / 2), ball.xPos, ball.yPos);
			g.setColor(ball.col);
			g.fillOval(ball.xPos - halfDia, ball.yPos - halfDia, Common.BALL_DIA, Common.BALL_DIA);
		}
		
		
	}

}
