package com.network.testing.gui;

import java.awt.Color;

public class Ball {
	public int id, xPos, yPos, xVel, yVel;
	public Color col; 
	
	public Ball () {
		
	}
	
	public Ball (int... vals) {
		id = vals[0];
		xPos = vals[1];
		yPos = vals[2];
		xVel = vals[3];
		yVel = vals[4];
	}
	public void update () {
		xPos += xVel;
		yPos += yVel;
		
		if (xPos < 0 || xPos > Common.SCREEN_WIDTH) {
			xVel = -xVel;
			xPos += (2 * xVel);
		}
		
		if (yPos < 0 || yPos > Common.SCREEN_HEIGHT) {
			yVel = -yVel;
			yPos += (yVel * 2);
		}
		
		if (xPos < 0) {
			xPos = 0 + Common.BALL_DIA;
		}
		
		if (yPos < 0) {
			yPos = 0 + Common.BALL_DIA;
		}
		
		if (xPos > Common.SCREEN_WIDTH) {
			xPos = Common.SCREEN_WIDTH - Common.BALL_DIA;
		}
		
		if (yPos > Common.SCREEN_HEIGHT) {
			yPos = Common.SCREEN_HEIGHT - Common.BALL_DIA;
		}
		
	}
	
	
	
}