package com.network.testing.gui;

public class Common {
	public static final int SCREEN_WIDTH = 400;
	public static final int SCREEN_HEIGHT = 300;
	
	public static final int NUM_BALLS = 5;
	public static final int BALL_DIA = 12;
}
