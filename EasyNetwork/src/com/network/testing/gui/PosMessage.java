package com.network.testing.gui;

import com.network.packing.Message;

public class PosMessage extends Message {

	public int id, x, y;
	
	public PosMessage () {
		super();
	}

	public PosMessage (int inId, int inX, int inY) {
		super();
		this.id = inId;
		this.x = inX;
		this.y = inY;
	}
	

}
