package com.network.testing.gui;

import java.io.IOException;

import com.network.ServerNetwork;
import com.network.exceptions.NetStartupException;
import com.network.profiles.NetworkProfile;


public class BallsServer {

	private ServerNetwork net = null;
	
	private boolean sendAllBallPosMsg = true;
	
	public BallsServer () throws InterruptedException, IOException, NetStartupException {
		
		net = new ServerNetwork();
		net.registerClass(PosMessage.class);
		net.registerClass(AllPosMsg.class);
		
		net.listen(45000);
		
		Ball[] balls = new Ball[Common.NUM_BALLS];
		
		for (int i = 0; i < balls.length; i++) {
			
			int xVel = (int)(Math.random() * 2) + 1;
			int yVel = (int)(Math.random() * 2) + 1;
			
			xVel = (Math.random() > 0.5? -xVel: xVel);
			yVel = (Math.random() > 0.5? -yVel: yVel);
			
			
			
			balls[i] = new Ball(i, (int)((Math.random() * Common.SCREEN_WIDTH) + 1), (int)((Math.random() * Common.SCREEN_WIDTH) + 1), xVel, yVel);
		}
		long nextSent = System.currentTimeMillis();
		long netRate = 50;
		
		
		while (true) {
		
			for (Ball ball: balls) {
				ball.update();
			}
			
			
			if (nextSent < System.currentTimeMillis()) {
				nextSent += netRate;
				
				for (NetworkProfile profile: net.getAllProfiles()) {
				
					
					
					if (sendAllBallPosMsg) {
	
						AllPosMsg msg = new AllPosMsg();
						msg.positions = new PosMessage[balls.length];
						
						for (Ball ball: balls) {
							msg.positions[ball.id] = new PosMessage(ball.id, ball.xPos, ball.yPos);
						}
						
						net.writeMessage(msg, profile);
						
					} else {
						
						for (Ball ball: balls) {
							net.writeMessage(new PosMessage(ball.id, ball.xPos, ball.yPos), profile);							
						}
					
					}
					
					net.flushMessages();
					//profile.lastSent = System.currentTimeMillis();
				}
				
			}
			
			//sim will run at 60fps
			Thread.sleep(17);
		}
		
		
		
	}
	
	
	public static void main (String[] args) throws NetStartupException, InterruptedException, IOException  {
		new BallsServer();
	}


	
	
}
	
