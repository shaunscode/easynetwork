package com.network.testing.gui;

import java.awt.Color;
import java.util.List;

import javax.swing.JFrame;

import com.network.ClientNetwork;
import com.network.exceptions.NetStartupException;
import com.network.packing.Message;


public class BallsClient extends JFrame	{
	
	private Color[] colors = {
			Color.RED,
			Color.BLUE,
			Color.GREEN,
			Color.CYAN,
			Color.LIGHT_GRAY,
			Color.GRAY,
			Color.DARK_GRAY,
			Color.YELLOW,
			Color.MAGENTA,
			Color.PINK,
			Color.ORANGE
	};

	private Ball[] balls = new Ball[Common.NUM_BALLS];
	private Display display = new Display(balls);
	
	
	public static void main (String[] args) throws NetStartupException, InterruptedException {
		
		new BallsClient();
		
	}
	
	public BallsClient () throws NetStartupException, InterruptedException {
		
		this.setTitle("Balls Client");
		this.setSize(Common.SCREEN_WIDTH, Common.SCREEN_HEIGHT);
		this.setLocation(10, 10);
		this.setContentPane(display);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		for (int i = 0; i < balls.length; i++) {
			balls[i] = new Ball();
			balls[i].id = i;
			balls[i].col = colors[i % colors.length];
		}

	
		ClientNetwork net = new ClientNetwork();
	///	net.setConnection(new ClientConnSim());
		
		net.registerClass(PosMessage.class);
		net.registerClass(AllPosMsg.class);
		

		long time = System.currentTimeMillis();
	
		net.connect("localhost", 45000);
		
				
		
		while (true) {
			Thread.sleep(50);
			List<Message> msgs = net.readMessages();
			for (Message msg: msgs) {
				
				if (msg.getClass().equals(AllPosMsg.class)) {
					handleMessage((AllPosMsg)msg);
				} else if (msg.getClass().equals(PosMessage.class)) { 
					handleMessage((PosMessage)msg);
				}
			}
			
			display.repaint();
		}
		
	}
	
	public void handleMessage(PosMessage inMessage) {
	
		PosMessage msg = (PosMessage)inMessage;
		balls[msg.id].xPos = msg.x;
		balls[msg.id].yPos = msg.y;
	
	}
	
	public void handleMessage(AllPosMsg inMessage) {
		
		for (PosMessage pos: inMessage.positions) {
		
			balls[pos.id].xPos = pos.x;
			balls[pos.id].yPos = pos.y;
			
		}
	}

}
