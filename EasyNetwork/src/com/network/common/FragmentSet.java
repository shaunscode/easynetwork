package com.network.common;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.network.exceptions.NetRunningFragmentException;
import com.network.packing.Fragment;

public class FragmentSet {
	
	private Logger log = Logging.createLogger("FragmentSet", Level.ALL, Level.INFO);

	private long timeCreated = System.currentTimeMillis();	//use to timeout and drop packets if it's taking too long.
	private int senderId = -1;	//id of the sender
	private byte setId = -1;	//id of the fragset
	private int setSize = 0;
	private int setTotalFragments = 0;
	private int setByteSize = -1;
	
	private Set<Fragment> fragSet = new TreeSet<Fragment>(new Comparator<Fragment>() {
		public int compare(Fragment fragmentOne, Fragment fragmentTwo) {
			return (fragmentOne.index < fragmentTwo.index? -1: 1);
		}
	});
	public int getSenderId () {
		return senderId;
	}
	
	public byte getId () {
		return setId;
	}
	public boolean isComplete () {
		return fragSet.size() >= setTotalFragments;
	}
	
	public void addFragment (Fragment inFragment) throws NetRunningFragmentException {
		log.finest("ENTERED");
		
		if (setSize == 0) {
			this.senderId = inFragment.msgSenderId;
			this.setId = inFragment.fragmentId;
			this.setTotalFragments = inFragment.totalFragments;
		} else if (inFragment.fragmentId != setId) {
			throw new NetRunningFragmentException(this, "Fragment being added (id: " + inFragment.fragmentId + ") does not match FragmentSet Id (id: " + setId + ")");
		} else if (inFragment.msgSenderId != senderId) {
			throw new NetRunningFragmentException(this, "Fragment being added (sender id: " + inFragment.msgSenderId + ") does not match FragmentSet sender Id (id: " + senderId + ")");
		}			
		fragSet.add(inFragment);
		this.setSize++;
		this.setByteSize += inFragment.data.length;
		log.finer("Fragment added to set id " + this.setId + " (fragment " + (inFragment.index + 1) + " of " + (inFragment.totalFragments) + ")");
	}
	
	public byte[] getData () throws NetRunningFragmentException {
		log.finest("ENTERED");
		if (!isComplete()) {
			throw new NetRunningFragmentException(this, "Attempting to retrieve incomplete Fragment set");
		}
		byte[] data = new byte[setByteSize + 1];	//+1 because the sizes addded to form setByteSize don't account for 0 being a populated element
		log.finer("combining Fragment set id " + this.setId);
		for (Fragment frag: fragSet) { 
			int dataIndex = frag.index * Constants.NET_FRAGMENT_SIZE;
			int len = frag.data.length;
			
			log.finer("\nabout to copy fragment byte array:\n\tsrc size: " + frag.data.length + " (" + (frag.data.length - 1) + " last index)" +
					"\n\tsrc pos: " + 0 + 
					"\n\tdest size: " + data.length + " (" + (data.length - 1) + " last index)" +
					"\n\tdest pos: " + dataIndex + " (first index to populate)" +
					"\n\tCopying " + len + " bytes (indices " + dataIndex + " to " + (dataIndex + len - 1) + " of dest will be populated) from fragment at index " + frag.index + " of " + (frag.totalFragments - 1)+ " fragment set indices");
			
			
			System.arraycopy(frag.data, 0, data, dataIndex, len);
		}
		return data;
	}
	
	public boolean isTimedOut () {
		if (System.currentTimeMillis() > timeCreated + Constants.NET_FRAGMENT_TIMEOUT_PERIOD) {
			log.fine("Fragment set id " + this.setId + " has timed out");
			return true;
		}
		return false;
	}
}
