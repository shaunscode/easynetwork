package com.network.common;

public class Constants {
	
	public static final byte KEY = 0;
	public static final byte VERSION = 0; 
	
	public static final int CONN_START_TIMEOUT_PERIOD = 15000;	//msecs to wait for Connection obj thread to start before deciding it isn't going to start
	
	public static final int PROFILE_SENT_PACKETS_BUFFER_LENGTH = 256;	//not locked to this size. 
	public static final int PROFILE_PACKET_ACK_BITS_LENGTH = 32;	//this is 32 because we use an int to store the ackBits values
	public static final int PROFILE_RTT_PINGS_BUFFER_LENGTH = 5;
	
	public static final int CLIENT_CONN_REQ_ATEMPTS = 20; 	//num times it will req connection before giving up
	public static final int CLIENT_CONN_REQ_INTERVAL = 3000;
	public static final int CLIENT_CONN_ACC_INTERVAL = CLIENT_CONN_REQ_INTERVAL / 2;
	//public static final int CLIENT_CONN_REQ_PERIOD = 60;
	
	public static final int NET_PROCESSING_INTERVAL = 1;	//msecs to sleep the main Network processing thread between loops
	public static final int NET_TIMEOUT_PERIOD = CONN_START_TIMEOUT_PERIOD + 10000;	//msecs to wait for a new net packet before deciding it's timedout
	public static final int NET_DATAGRAM_BUFFER_SIZE = 1400;		//datagram buffer for incoming data
	public static final int SERVER_PING_INTERVAL = 1000;	//msecs between Pings sent by the default Network runPingLoop() method any connections (client->server or server->all clients)
	public static final int NET_BYTEBUFFER_UNDERFLOW_SIZE = 4;

	public static final long NET_FRAGMENT_TIMEOUT_PERIOD = 10000;	//millis a fragment set will be deemed valid for. is checked each processing loop. 
	public static final int NET_FRAGMENT_SIZE = Constants.NET_DATAGRAM_BUFFER_SIZE / 2;
	
	public static enum ConnectionState { 
		REQUESTING, 
		CONNECTING,
		CONNECTED, 
		DISCONNECTED
	}

	
}
