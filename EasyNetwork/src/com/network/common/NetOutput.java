package com.network.common;

import java.io.PrintStream;

public class NetOutput {
	
	private static PrintStream out = System.out;
	private static PrintStream err = System.err;
		
	public static void info (Object inObj, String inString) {
		out.println("Info @ " + inObj.getClass().getSimpleName() + ":: " + inString);
	}

	public static void warn (Object inObj, String inString) {
		out.println("WANRING @ " + inObj.getClass().getSimpleName() + ":: " + inString);
	}

	public static void err (Object inObj, String inString) {
		err.println("### ERROR @ " + inObj.getClass().getSimpleName() + ":: " + inString);
	}

	public static void exception (Object inObj, Exception inException) {
		err.println("### EXCEPTION @ " + inObj.getClass().getSimpleName() + ":: " + inException.getClass().getSimpleName() + " Exception: " + inException.getMessage());
		inException.printStackTrace();
	}

	
}
