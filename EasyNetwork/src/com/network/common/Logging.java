package com.network.common;

import java.util.HashMap;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

//just a place to keep track of the loggers per Class
public class Logging {
	
	private static HashMap<String, Logger> loggers = new  HashMap<String, Logger>();
	
	public static Logger createLogger (String inName, Level inLogLevel, Level inOutputLevel) {
		System.setProperty("java.util.logging.SimpleFormatter.format", "%4$s @ %2$s() :: %5$s%6$s%n");
		Logger logger = Logger.getLogger(inName);
		logger.setLevel(inLogLevel);
		Handler handler = new ConsoleHandler();
		handler.setLevel(inOutputLevel);
		logger.setUseParentHandlers(false);
		logger.addHandler(handler);
		loggers.put(inName,  logger);
		return logger;
	}
	
}
