package com.network.messages;

import com.network.packing.Message;

public class PongMsg extends Message {

	public long timePingCreated;
	public long timePongCreated;
	public int pingId;

}
