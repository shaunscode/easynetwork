package com.network.messages;

import com.network.packing.Message;

public class ConnRequestMsg extends Message {

	public byte[] reqFromAddress;
	public int reqFromPort;
	
}
