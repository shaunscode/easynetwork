package com.network;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.network.common.Constants;
import com.network.common.Logging;
import com.network.common.NetOutput;
import com.network.connection.ConnectionState;
import com.network.exceptions.NetRunningSerializationException;
import com.network.exceptions.NetStartupException;
import com.network.messages.ConnAcceptedMsg;
import com.network.messages.ConnRequestMsg;
import com.network.messages.ConnResponseMsg;
import com.network.messages.DisconnectMsg;
import com.network.messages.PingMsg;
import com.network.messages.PongMsg;
import com.network.packing.Message;
import com.network.profiles.ClientProfile;
import com.network.profiles.NetworkProfile;
import com.network.utils.NetUtils;

public class ServerNetwork extends Network {	
		
	private Logger log = Logging.createLogger("ServerNetwork", Level.ALL, Level.FINE);
	
	//exposes network listening to Server instance only
	public void listen (int inPort) throws NetStartupException  {
		super.startup(inPort);
		localProfile.profileId = 234;
		super.startNetGraph("Server clients");
	}
	
	
	public boolean isListening () {
		return isRunning();
	}
	
	
	private ClientProfile buildNewProfile (InetAddress inAddr, int inPort) {
		ClientProfile profile = new ClientProfile();
		profile.address = inAddr;
		profile.port = inPort;
		profile.profileId = (int)(Math.random() * Integer.MAX_VALUE);
		profile.state = ConnectionState.CONNECTING;
		profile.lastActive = System.currentTimeMillis();
		log.fine("New profile created (id " + profile.profileId + ") for " + inAddr + ":" + inPort);
		return profile;
	}
	
	private void writePingMsg (ClientProfile inProfile) {
		if (inProfile.state == ConnectionState.CONNECTED) {
			PingMsg ping = new PingMsg();
			ping.timeCreated = System.nanoTime();
			writeMessage(ping, inProfile);
		}
	}
	
	private void sendPingMsg (ClientProfile inProfile) {
		writePingMsg(inProfile);
		flushMessages(inProfile);
	}
	
	public void writeBroadcastMessage (Message inMsg) {
		for (NetworkProfile profile: profiles.values()) {
			writeMessage(inMsg, profile);
		}
	}
	

	public void flushMessages () {
		for (NetworkProfile profile: profiles.values()) {
			flushMessages(profile);
		}
	}
	
	public void flushMessages (NetworkProfile inProfile) {
		flushMessages(inProfile, 0);
	}
	
	private boolean isConnRequesterConnected (InetAddress inAddr, int inPort) {
		ClientProfile profile = (ClientProfile)getProfileByAddress(inAddr, inPort);
		if (profile == null) {
			return false;
		}
		if (profile.state == ConnectionState.CONNECTED) {
			return true;
		}
		return false;
	}

	private boolean isConnRequesterConnecting (InetAddress inAddr, int inPort) {
		ClientProfile profile = (ClientProfile)getProfileByAddress(inAddr, inPort);
		if (profile == null) {
			return false;
		}
		if (profile.state == ConnectionState.CONNECTING) {
			return true;
		}
		return false;
	}
	
	private void sendConnectionResponse (NetworkProfile inProfile) {
		//build response, assign new profile id
		ConnResponseMsg connResMsg = new ConnResponseMsg();
		connResMsg.newProfileId = inProfile.profileId;		//send the response
		writeMessage(connResMsg, inProfile);
		flushMessages(inProfile);
		
	}
	
	/*
	 * Clients request connection, sever creates a profile and sends back their id and adds them to the connecting clients array
	 */
	protected void handleConnReqMsg (ConnRequestMsg inConnReqMsg) {
		log.info("ConnRequestMsg received");
		
		InetAddress reqAddress = null;
		try {
			reqAddress = InetAddress.getByAddress(inConnReqMsg.reqFromAddress);
		} catch (UnknownHostException e1) {
			log.warning("Failed to handle connection: Bad InetAddress from requestor: " + e1.getMessage());
			return;
		}
		int reqPort = inConnReqMsg.reqFromPort;
		
		
		ClientProfile profile = (ClientProfile)getProfileByAddress(reqAddress, reqPort);
		if (profile == null) {
			//fresh new connecting?
			log.info("ConnRequestMsg from new address " + reqAddress + ":" + reqPort + ". Building new client.");
			profile = buildNewProfile(reqAddress, reqPort);
			profile.state = ConnectionState.CONNECTING;
			sendConnectionResponse(profile);
			//successful? add to profiles map
			profiles.put(profile.profileId, profile);
			log.fine("profile " + profile.profileId + " added to profiles map");
		} else {
			if (profile.state == ConnectionState.CONNECTED) {
				NetOutput.info(this, "Request is from connected address " + reqAddress + ":" + reqPort + " (ProfileId: " + profile.profileId + "). Sending ping.");
				sendPingMsg(profile);
			} else if (profile.state == ConnectionState.CONNECTING) {
				NetOutput.info(this, "Request is from connecting address " + reqAddress + ":" + reqPort + " (ProfileId: " + profile.profileId + "). Resending response.");
				sendConnectionResponse(profile);
			}
		}
	}


	/*
	 * Client should send back the packet with their profile id that was given also
	 * from here out server will only use profile Id to find address/port to send to
	 * server takes profile (based on profile id) out of connecting and places into clients
	 */
	protected void handleConnAccMsg (ConnAcceptedMsg inMsg) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("ConnAcceptedMsg received");
		
		//Is it from someone in the process of connecting
		ClientProfile profile = (ClientProfile)profiles.get(inMsg.msgSenderId);
		
		//yes it was..
		if (profile == null) {
			sb.append(" from non-connected profile " + inMsg.msgSenderId + ". Ignoring.");
			
		} else if (profile.state == ConnectionState.CONNECTING) {
			sb.append(" profile id " + inMsg.msgSenderId +" ");
			((ClientProfile)profile).state = ConnectionState.CONNECTED;
			profile.lastActive = System.currentTimeMillis();
			sb.append(" from connecting profile id " + inMsg.msgSenderId + ". Profile state is now connected.");
		} else if (profile.state == ConnectionState.CONNECTED) {
			sb.append(" already connected profile id " + inMsg.msgSenderId +". Sending ping.");
			sendPingMsg(profile);
		} else {
			sb.append(", but wasn't handled? what went wrong?");
		}
		
		log.info(sb.toString());

	}

	//checks profile is connected and has activity within netProfileTimeoutPreiod range
	protected boolean isTimedOut (NetworkProfile profile) {
		boolean isConnected = (((ClientProfile)profile).state == ConnectionState.CONNECTED);
		boolean isInactive = (((ClientProfile)profile).lastActive + Constants.NET_TIMEOUT_PERIOD) < System.currentTimeMillis();
		boolean timedOut = isConnected && isInactive;
		if (timedOut) {
			log.info("profile id " + profile.profileId + " has timed out.");
		}
		return timedOut;
	}	
	

	
	protected void handleConnResponse(ConnResponseMsg inPacket) {
		//not applicaticable to the server instance
	}
	
	@Override
	protected void runNetSpecificProcesses () {
		long sysTime = System.currentTimeMillis();
			
		ClientProfile profile = null;
		Iterator<Entry<Integer, NetworkProfile>> iter = profiles.entrySet().iterator();
		
		Entry<Integer, NetworkProfile> entry = null;
		while (iter.hasNext()) {
		
			entry = iter.next();
			profile = (ClientProfile)entry.getValue();
			
			if (profile.state == ConnectionState.CONNECTING) { 
				sendConnectionResponse(profile);
			}

			//ping first, remove if timedout later
			if ((profile.lastPinged + Constants.SERVER_PING_INTERVAL) < sysTime){
				writePingMsg(profile);	
				flushMessages();			
				profile.lastPinged = sysTime;
			}
			
			if (isTimedOut(profile) ) {
				writeMessage(new DisconnectMsg(), profile);	//courtesy msgs in case they come through afterwards
				flushMessages(profile);
				iter.remove();
			} 
			
			
			
			
		}
	}


	

	@Override
	protected boolean handleNetworkMsg (Message inMsg) {
		
		if (inMsg.getClass().equals(ConnRequestMsg.class)) {
			log.finer("ConnRequestMsg received by ServerNetwork..");
			handleConnReqMsg((ConnRequestMsg) inMsg);
			return true;
		} else if (inMsg.getClass().equals(ConnAcceptedMsg.class)) {
			log.finer("ConnAcceptedMsg received by ServerNetwork..");
			handleConnAccMsg((ConnAcceptedMsg) inMsg);
			return true;
		} else if (inMsg.getClass().equals(PongMsg.class)) {
			log.finer("PongMsg received by ServerNetwork..");
			handlePongMsg((PongMsg) inMsg);
			return true;
		}
		log.finer("Msg " + inMsg.getClass().getSimpleName() + " not being handled by ServerNetwork..");
		return false;
	}


	
	protected void handlePongMsg (PongMsg inPongMsg) {
		log.finer("Ping RTT: " + (NetUtils.getNanoAsMilli(inPongMsg.timePongCreated- inPongMsg.timePingCreated)));
		
		ClientProfile profile = (ClientProfile)this.getProfileById(inPongMsg.msgSenderId);
		profile.lastActive = System.currentTimeMillis();
		
		int index = inPongMsg.pingId % Constants.PROFILE_RTT_PINGS_BUFFER_LENGTH;
		profile.pings[index] = inPongMsg.timePongCreated - inPongMsg.timePingCreated;
		
		int totalRTT = 0;
		for (int i = 0; i < Constants.PROFILE_RTT_PINGS_BUFFER_LENGTH; i++) {
			totalRTT += profile.pings[i];
		}
		profile.RTT = NetUtils.getNanoAsMilli(totalRTT / Constants.PROFILE_RTT_PINGS_BUFFER_LENGTH);
		log.finer("Avg RTT: "+ profile.RTT + "ms");
	}
	
}
