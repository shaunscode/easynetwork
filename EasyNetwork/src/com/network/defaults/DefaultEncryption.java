package com.network.defaults;

import java.net.DatagramPacket;

import com.network.interfaces.Encryption;

public class DefaultEncryption implements Encryption {

	@Override
	public DatagramPacket encrypt(DatagramPacket inDatagramPacket) {
		return inDatagramPacket;
	}

	@Override
	public DatagramPacket decrypt(DatagramPacket inDatagramPacket) {
		return inDatagramPacket;
	}

}
