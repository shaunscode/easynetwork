package com.network.serialization;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.network.common.Constants;
import com.network.common.Logging;
import com.network.exceptions.NetRunningSerializationException;
import com.network.messages.ConnAcceptedMsg;
import com.network.messages.ConnRequestMsg;
import com.network.messages.ConnResponseMsg;
import com.network.messages.DisconnectMsg;
import com.network.messages.HeaderMsg;
import com.network.messages.PingMsg;
import com.network.messages.PongMsg;
import com.network.packing.DeltaMessage;
import com.network.packing.Fragment;
import com.network.packing.Message;
import com.network.packing.PacketHeader;
import com.network.utils.BitQueue;

public class Serializer {
	
	private static Logger log = Logging.createLogger("Serializer", Level.ALL, Level.FINER);
	
	private ArrayPopulation arrs = new ArrayPopulation();
	
	private ByteBuffer serializerBB = ByteBuffer.allocate(Constants.NET_DATAGRAM_BUFFER_SIZE);
		
	private static Serializer instance = null;
	
	private HashMap<Byte, Class<? extends Object>> byteToClassMap = new HashMap<Byte, Class<? extends Object>>();
	private HashMap<Class<? extends Object>, Byte> classToByteMap = new HashMap<Class<? extends Object>, Byte>();
	private Byte classIdCounter = -127;
	private static List<String> ignoredFieldNames = new ArrayList<String>();
	private HashMap<Class, Message> baseDeltaMsgsOut = new HashMap<Class, Message>();
	

	
	public Serializer () {
		log.finest("ENTERED");
		//log.setLevel(Level.INFO);
		//LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME).setLevel(Level.FINEST);
		
		registerClass(PacketHeader.class);
		registerClass(ConnRequestMsg.class);
		registerClass(ConnResponseMsg.class);
		registerClass(ConnAcceptedMsg.class);
		registerClass(PingMsg.class);
		registerClass(PongMsg.class);
		registerClass(DisconnectMsg.class);		
		registerClass(Fragment.class);		


		//from Message.class
		Serializer.addIgnoredFieldName("msgSenderId");
		Serializer.addIgnoredFieldName("msgPacketId");
		Serializer.addIgnoredFieldName("msgReliable");
		
		
		//from PacketHeader - these are populated sender side for building the DatagramPacket. No need to send them to the receiver
		Serializer.addIgnoredFieldName("sendingToAddress");
		Serializer.addIgnoredFieldName("sendingToPort");
		
	
	}
	
	public static Serializer getInstance () {
		if (instance == null) {
			instance = new Serializer();
		}
		return instance;
	}
	
	public void registerClass (Class<? extends Object> inClass) {
		log.finest("ENTERED");
		byte b = classIdCounter++;
		byteToClassMap.put(b,  inClass);
		classToByteMap.put(inClass, b);
		log.info("Message type " + b + " registered with " + inClass.getSimpleName());
	}

	public static void addIgnoredFieldName (String inName) {
		ignoredFieldNames.add(inName);
	}
		

	public byte[] serialize (Message inMsg) throws NetRunningSerializationException {
		int size = getObjectByteSize(inMsg);
		serializerBB = ByteBuffer.allocate(size + Constants.NET_BYTEBUFFER_UNDERFLOW_SIZE);
		byte[] data = serialize(serializerBB, inMsg);	
		return preappendClassType(data, inMsg.getClass());
	}
	
	
	private byte[] serialize (ByteBuffer inBB, Object inObj) throws NetRunningSerializationException {
		log.finest("ENTERED");
		try {
			Field[] fs = inObj.getClass().getFields();
					
			for (int i = 0; i < fs.length; i++) {
				
				if (ignoredFieldNames.contains(fs[i].getName())) {
					continue;
				}
				
				//System.out.print("\tField num" + i + ": " + fs[i].getName() + ", ");			
				if (fs[i].getType().isArray()) {
					//System.out.println(" is an array!");
					try {
						serializeArray(inBB, fs[i].get(inObj));
					} catch (ArrayIndexOutOfBoundsException | IllegalArgumentException | IllegalAccessException e) {
						throw new NetRunningSerializationException(this, "Failed to serialize an Array: " + e.getMessage());
					}
					
				} else {
					try {
						serializeByType(fs[i].getType().getSimpleName(), inBB, fs[i].get(inObj));
					} catch (IllegalArgumentException | IllegalAccessException e) {
						throw new NetRunningSerializationException(this, "Failed to serialize a type: " + e.getMessage());
					}
					
				}
			}		
			
			byte[] byteValue = new byte[inBB.position()];
			System.arraycopy(inBB.array(), 0, byteValue, 0, byteValue.length);
			
			return byteValue;
		
		} catch (BufferOverflowException e) {
			throw new NetRunningSerializationException(this, "Failed to serialize a type: " + e.getMessage());
		}
	}
	
	
private void serializeArray (ByteBuffer inBB, Object inArr) throws NetRunningSerializationException {
		log.finest("ENTERED");
		if (inArr == null) {
			inBB.putInt(-1);
			return;
		} 
			
		int len = Array.getLength(inArr);
		
		//write the array len to buffer to know how many elements to read when rebuilding
		inBB.putInt(len);
		//System.out.println("Serializing array " + inArr.getClass().getSimpleName() + ", len: " + len + " of components " + inArr.getClass().getComponentType());
		for (int i = 0; i < len; i++) {
			try {
				serializeByType(inArr.getClass().getComponentType().getSimpleName(), inBB, Array.get(inArr, i));
			} catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
				log.severe("Exception thrown: " + e);
				throw new NetRunningSerializationException(this, "Failed to serialize Array");
			}
		}
		
	}

	
	private void serializeByType (String inFieldType, ByteBuffer inBB, Object inObj) throws NetRunningSerializationException {
		log.finest("ENTERED");
		try {
		switch (inFieldType) {
		case "boolean":
			//System.out.println("writing boolean.." + (boolean)inObj);
			boolean value = (boolean)inObj;
			if (value) {
				inBB.put((byte)1);
			} else {
				inBB.put((byte)0);
			}	
			break;
			
		case "byte":
			//System.out.println("writing byte.." + (byte)inObj);
			inBB.put((byte)inObj);
			break;
		
		case "char":
			//System.out.println("writing char.." + (char)inObj);
			inBB.putChar((char)inObj);
			break;
				
		case "short":
			//System.out.println("writing short, value " + (short)inObj);
			inBB.putShort((short)inObj);
			break;
			
		case "int":
			//System.out.println("writing int, value " + (int)inObj);
			inBB.putInt((int)inObj);
			break;
		
		case "float":
			//System.out.println("writing float.." + (float)inObj);
			inBB.putFloat((float)inObj);
			break;
		
		case "long":
			//System.out.println("writing long.." + (long)inObj);
			inBB.putLong((long)inObj);
			break;
		
		case "double":
			//System.out.println("writing double.." + (double)inObj);
			inBB.putDouble((double)inObj);
			break;

		case "String":
			String str = (String)inObj;
			//System.out.println("writing String: " + str);
			
			if (str == null) {
				//-1 indicates null values
				inBB.putInt(-1);
				
			} else {
				//write the string len to the the buffer for rebuilding
				inBB.putInt(str.length());
				inBB.put(str.getBytes());
			}
			break;
			
		default:
			//System.out.println("writing Obj..");
		
				//need to indicate to across the network whether an obj in the class is NULL
				//do this by sending a byte 0/1 to indicate whether to retrieve te ob or assign it
				//to null on the other end.
				if (inObj == null) {
					inBB.put((byte)0);
					return;
				} else {
					inBB.put((byte)1);
					try {
						serialize(inBB, inObj);
					} catch (BufferOverflowException e) {
						throw new NetRunningSerializationException(this, "Failed to serialize an Object field.");
					}
				}
				
			
		
				//
		}	
		
		} catch (BufferOverflowException e) {
			throw new NetRunningSerializationException(this, "Failed to serialize field.", e);
		}
		
	}

	
	private void deserialize (ByteBuffer inBB, Object inObj) throws NetRunningSerializationException {
		log.finest("ENTERED");
		Field[] fs = inObj.getClass().getFields();
		
		for (int i = 0; i < fs.length; i++) {		
			

			if (ignoredFieldNames.contains(fs[i].getName())) {
				continue;
			}
			//System.out.print("\n\tField " + fs[i].getName() + "...");
			if (fs[i].getType().isArray()) {
				//System.out.println(" is an array!");
				try {
					deserializeArray(inBB, fs[i], inObj);
				} catch (ArrayIndexOutOfBoundsException | IllegalArgumentException
						| BufferUnderflowException e) {
					log.severe("Exception thrown: " + e);
					throw new NetRunningSerializationException(this, "Failed to deserialize a type: " + e.getMessage());
				}
				continue;
			} else {
				deserializeByField(fs[i], inBB, inObj);
			}
			
		}
	
	}
	

	private void deserializeByField (Field inField, ByteBuffer inBB, Object inObj) throws NetRunningSerializationException {
		//rebuilding the obj is a bit diff from reading it
		//as reflection needs the Field and the Object to which the field belongs and is setting the value of

		log.finest("ENTERED");
		try {
			switch (inField.getType().getSimpleName()) {
			case "boolean":
				//System.out.print("reading boolean..");
				boolean value = inBB.get() == 1? true: false;
				inField.setBoolean(inObj, value);
				break;
				
			case "byte":
				//System.out.print("reading byte..");
				inField.setByte(inObj, inBB.get());
				break;
			
			case "char":
				//System.out.print("reading char..");
				inField.setChar(inObj, inBB.getChar());
				break;
					
			case "short":
				//System.out.print("reading short..");
				inField.setShort(inObj, inBB.getShort());
				break;
				
			case "int":
				//System.out.print("reading int..");
				inField.setInt(inObj, inBB.getInt());
				break;
				
			case "float":
				//System.out.print("reading float..");
				inField.setFloat(inObj, inBB.getFloat());
				break;
			
			case "long":
				//System.out.print("reading long..");
				inField.setLong(inObj, inBB.getLong());
				break;
			
			case "double":
				//System.out.print("reading double..");
				inField.setDouble(inObj, inBB.getDouble());
				break;
	
			case "String": 
				
				int strLen = inBB.getInt();
				//System.out.print("reading String.. len = " + strLen);
				if (strLen == -1) {
					return;
				} else if (strLen == 0) {
					inField.set(inObj, new String());
				} else {
					byte[] str = new byte[strLen];
					inBB.get(str);
					inField.set(inObj, new String(str));
				}
				break;
	
			default:
				//System.out.println("reading Obj..");
		
					
					Object obj = inField.getType().newInstance();
					boolean isNull = inBB.get() == 0? true: false;
					if (isNull) {
						return;
					}
			
					//populate all the fields in the obj var of inObj
					deserialize(inBB, obj);
					
					//assign the populated obj to the inObj var is belongs to.
					inField.set(inObj, obj);
					
					
			
				
			}
		} catch (BufferOverflowException | InstantiationException | IllegalAccessException e) {
			log.severe("Exception thrown: " + e);
			throw new NetRunningSerializationException(this, "Failed to deserialize a field " + inField.getType().getClass().getSimpleName());
		}
	}
		
	private void deserializeArray (ByteBuffer inBB, Field inField, Object inObj) throws NetRunningSerializationException {
	/*
		rebuilding an array..
		inBytes = the data
		inField = the array 'field' we call set(obj) on 
		inObj = the obj we're populating
	*/
		
		log.finest("ENTERED");
		
		//read num of arr elements there are to read
		int len = inBB.getInt();
		//System.out.println("deserializeArray: arr len: " + len);
		
		if (len == -1) {
			try {
				inField.set(inObj, null);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				log.severe("Exception thrown: " + e);
				throw new NetRunningSerializationException(this, "Failed to set Array to null");
			}
			return;
		}
		
		//read the to that the array actually is.
		Class<?> arrClass = inField.getType().getComponentType();
		//System.out.println("deserializeArray: getComponentType: " + arrClass);
		Object newArr = Array.newInstance(arrClass, len);
		//System.out.println("new arr created: " + newArr.getClass().getSimpleName() + ", of component: " + newArr.getClass().getComponentType());
		
		switch (arrClass.getSimpleName()) {
		case "boolean":
			arrs.populateBooleanArray(inBB, newArr, len);
			break;
			
		case "byte":
			//System.out.print("reading byte..");
			inBB.get((byte[])newArr);
			break;
		
		case "char":
			arrs.populateCharArray(inBB, newArr, len);
			break;
				
		case "short":
			arrs.populateShortArray(inBB, newArr, len);
			break;
			
		case "int":
			arrs.populateIntArray(inBB, newArr, len);
			break;
			
		case "float":
			arrs.populateFloatArray(inBB, newArr, len);
			break;
		
		case "long":
			arrs.populateLongArray(inBB, newArr, len);
			break;
		
		case "double":
			arrs.populateDoubleArray(inBB, newArr, len);
			break;

		case "String": 
			
			arrs.populateStringArray(inBB, newArr, len);
			break;

		default:
			
			arrs.populateObjectArray(inBB, newArr, len);
			
			break;
			
			
		}
		
		//System.out.println("Setting new arr back to populate obj");
		try {
			inField.set(inObj, newArr);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			log.severe("Exception thrown: " + e);
			throw new NetRunningSerializationException(this, "Failed to set Array field");
		}
	}
		

	public static int getObjectByteSize(Object inObj) throws NetRunningSerializationException {	
		log.finest("ENTERED");
		
		int size = 0;
		
		Field[] fs = inObj.getClass().getFields();
		
/*		if (fs == null) {
			fs =  inObj.getClass().getFields();
			fieldsCache.put(inObj.getClass().getName(), fs);
		}*/
				
		for (int i = 0; i < fs.length; i++) {
			//System.out.print("\tField num" + i + ": " + fs[i].getName() + ", ");			
			

			if (ignoredFieldNames.contains(fs[i].getName())) {
				continue;
			}
			
			
			 if (fs[i].getType().isArray()) {
				//System.out.println(" is an array!");
				try {
					size += getArrayByteSize(fs[i].get(inObj));
				} catch (ArrayIndexOutOfBoundsException | IllegalArgumentException | IllegalAccessException e) {
					log.severe("Exception thrown calling getArrayByteSize(): " + e);
					throw new NetRunningSerializationException(null, "Failed to serialize an Array: " + e.getMessage());
				}
				
			} else {
				try {
					size += getFieldByteSize(fs[i].getType().getSimpleName(), fs[i].get(inObj));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					log.severe("Exception thrown: " + e);
					throw new NetRunningSerializationException(null, "Failed to serialize a type: " + e.getMessage());
				}
				
			}
		}		
		log.finer("calc'ed size for input object " + inObj.getClass().getSimpleName() + " is: " + size + " bytes");
		return size;
	}
	

	private static int getFieldByteSize(String inFieldType, Object inObj) throws NetRunningSerializationException {
		log.finest("ENTERED");
		int size = 0;
		
		try {
			switch (inFieldType) {
			case "boolean":
				//System.out.println("writing boolean.." + (boolean)inObj);
				size += 1;
				break;
				
			case "byte":
				size += 1;
				break;
			
			case "char":
				size += 2;
				break;
					
			case "short":
				size += 2;
				break;
				
			case "int":
				size += 4;
				break;
			
			case "float":
				size += 4;
				break;
			
			case "long":
				size += 8;
				break;
			
			case "double":
				size += 8;
				break;

			case "String":
				String str = (String)inObj;
				size += 4;	//still add 4 bytes because our serializer will put the Integer -1 to denote it is null or the length that follows
					
				if (str != null) {
					size += str.length();
				}
				break;
				
			default:
				//+1 as we'd put a 0 or 1 byte to denote null/not null when serializing
				size += 1;
				
				if (inObj != null) {
					size += getObjectByteSize(inObj);
				}
									//
			}	
			
			} catch (BufferOverflowException e) {
				throw new NetRunningSerializationException(null, "Failed to serialize field.", e);
			}
		return size;
	}
	

	private static int getArrayByteSize(Object inArr) throws NetRunningSerializationException {
		log.finest("ENTERED");
		//always have a size of 4 bytes for arrays, as when serializing it's either -1 to denote the array is null, or the number of elements it has (placed as an int, so four bytes)
		int size = 4;
		if (inArr == null) {
			return size;
		} 
		
		int len = Array.getLength(inArr);
		
		//System.out.println("Serializing array " + inArr.getClass().getSimpleName() + ", len: " + len + " of components " + inArr.getClass().getComponentType());
		for (int i = 0; i < len; i++) {
			try {
				size += getFieldByteSize(inArr.getClass().getComponentType().getSimpleName(), Array.get(inArr, i));
			} catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
				log.severe("Exception thrown: " + e);
				throw new NetRunningSerializationException(null, "Failed to serialize Array");
			}
		}
		return size;

	}


	public byte[] deltaSerialize(Message inBaseMsg, Message inNextMsg) throws NetRunningSerializationException {

		BitQueue delta = new BitQueue(128);
		
		int size = getObjectByteSize(inNextMsg);
		serializerBB = ByteBuffer.allocate(size + Constants.NET_BYTEBUFFER_UNDERFLOW_SIZE);
		
		//call to delta compress it
		byte[] bytes = deltaSerialize(serializerBB, inBaseMsg, inNextMsg, delta);
		
		System.out.println("Delta::" + delta.toString());
		
		return bytes;
	}
	

	public byte[] deltaSerialize (ByteBuffer inBB, Object inBaseObj, Object inNextObj, BitQueue inDelta) throws NetRunningSerializationException {
		log.finest("ENTERED");
		try {
			
		
			try {
				if (inBaseObj == null) {
					inBaseObj = inNextObj.getClass().newInstance();
				}
				if (inNextObj == null) {
					inNextObj = inNextObj.getClass().newInstance();
				}
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullPointerException e) {
				throw new NetRunningSerializationException(this, "failed trying to delta compress/serialize two null objs");
			}
			

			
			
			Field[] fs = inBaseObj.getClass().getFields();
					
			for (int i = 0; i < fs.length; i++) {
				
				if (ignoredFieldNames.contains(fs[i].getName())) {
					continue;
				}
				
				//System.out.print("\tField num" + i + ": " + fs[i].getName() + ", ");			
				if (fs[i].getType().isArray()) {
					//System.out.println(" is an array!");
					try {
						deltaSerializeArray(inBB, fs[i].get(inBaseObj), fs[i].get(inNextObj), inDelta);
					} catch (ArrayIndexOutOfBoundsException | IllegalArgumentException | IllegalAccessException e) {
						throw new NetRunningSerializationException(this, "Failed to delta decompress/deserialize an Array: " + e.getMessage());
					}
					
				} else {
					try {
						deltaSerializeByType(fs[i].getType().getSimpleName(), inBB, fs[i].get(inBaseObj), fs[i].get(inNextObj), inDelta);
					} catch (IllegalArgumentException | IllegalAccessException e) {
						throw new NetRunningSerializationException(this, "Failed to delta decompress/serialize a type: " + e.getMessage());
					}
					
				}
			}		

			
			byte[] deltaBits = inDelta.toByteArray();
			byte[] byteValue = new byte[inBB.position() + deltaBits.length + 2];
			int deltaBitsByteLen = deltaBits.length;
			
			//should be able to read this as a short by the byte buffer on recieving end.
			byteValue[1] = (byte) (deltaBitsByteLen & 0xff);
			byteValue[0] = (byte) ((deltaBitsByteLen >> 8) & 0xff);
			
			
			System.arraycopy(deltaBits, 0, byteValue, 2 , deltaBits.length);
			System.arraycopy(inBB.array(), 0, byteValue, 2 + deltaBits.length , inBB.position());
			
			return byteValue;
		
		} catch (BufferOverflowException e) {
			throw new NetRunningSerializationException(this, "Failed to serialize a type: " + e.getMessage());
		}
	}
	

	private void deltaSerializeByType (String inFieldType, ByteBuffer inBB, Object inBaseObj, Object inNextObj, BitQueue inDelta) throws NetRunningSerializationException {
		log.finest("ENTERED");
		try {
			switch (inFieldType) {
			case "boolean":
				//System.out.println("writing boolean.." + (boolean)inObj);
				if (((boolean)inNextObj) != ((boolean)inBaseObj)) {
					inDelta.add(true);
					boolean value = (boolean)inNextObj;
					if (value) {
						inBB.put((byte)1);
					} else {
						inBB.put((byte)0);
					}	
				} else {
					inDelta.add(false);
				}
				break;
				
			case "byte":
				//System.out.println("writing byte.." + (byte)inObj);
				if (((byte)inNextObj) != ((byte)inBaseObj)) {
					inDelta.add(true);
					inBB.put((byte)inNextObj);
				} else {
					inDelta.add(false);
				}
				break;
			
			case "char":
				//System.out.println("writing char.." + (char)inObj);
				if (((char)inNextObj) != ((char)inBaseObj)) {
					inDelta.add(true);
					inBB.putChar((char)inNextObj);
				} else {
					inDelta.add(false);
				}
				break;
					
			case "short":
				//System.out.println("writing short, value " + (short)inObj);
				if (((short)inNextObj) != ((short)inBaseObj)) {
					inDelta.add(true);
					inBB.putShort((short)inNextObj);
				} else {
					inDelta.add(false);
				}
				break;
				
			case "int":
				//System.out.println("writing int, value " + (int)inObj);
				if (((int)inNextObj) != ((int)inBaseObj)) {
					inDelta.add(true);
					inBB.putInt((int)inNextObj);
				} else {
					inDelta.add(false);
				}
				break;
			
			case "float":
				//System.out.println("writing float.." + (float)inObj);
				if (((float)inNextObj) != ((float)inBaseObj)) {
					inDelta.add(true);
					inBB.putFloat((float)inNextObj);
				} else {
					inDelta.add(false);
				};
				break;
			
			case "long":
				//System.out.println("writing long.." + (long)inObj);
				if (((long)inNextObj) != ((long)inBaseObj)) {
					inDelta.add(true);
					inBB.putLong((long)inNextObj);
				} else {
					inDelta.add(false);
				}
				break;
			
			case "double":
				//System.out.println("writing double.." + (double)inObj);
				if (((double)inNextObj) != ((double)inBaseObj)) {
					inDelta.add(true);
					inBB.putDouble((double)inNextObj);
				} else {
					inDelta.add(false);
				}
				break;
	
			case "String":
				String baseStr = (String)inBaseObj;
				String nextStr = (String)inNextObj;
				//System.out.println("writing String: " + str);
				
				if (baseStr == null && nextStr == null) {
					//-1 indicates null values
					inDelta.add(false);
					
				} else if (baseStr != null && nextStr == null) {
					inDelta.add(true);
					inBB.putInt(-1);	//-1 denotes a null value
				} else if (baseStr == null && nextStr != null) {
					inDelta.add(true);
					inBB.putInt(nextStr.length());
					inBB.put(nextStr.getBytes());
				} else if (baseStr != null && nextStr != null) {
					if (baseStr.equals(nextStr)) {
						inDelta.add(false);
					} else {
						inDelta.add(true);
						inBB.putInt(nextStr.length());
						inBB.put(nextStr.getBytes());
					}
				} else {
					inDelta.add(false);
					String err = "ERROR: Serializer.deltaSerializeByType() failed IF/ELSE statements - This is final ELSE msg - should not have reached here.";
					inBB.putInt(err.length());
					inBB.put(err.getBytes());
				}
				break;
				
			default:
				//System.out.println("writing Obj..");
				if (inBaseObj == null && inNextObj == null) {				//-1 indicates null values
					inDelta.add(false);
					
				} else if (inBaseObj != null && inNextObj == null) {
					inDelta.add(true);
					inBB.putInt(-1);	//-1 denotes a null value
				} else if (inBaseObj == null && inNextObj != null) {
					inDelta.add(true);
					deltaSerialize(inBB, inBaseObj, inNextObj, inDelta);
				} else if (inBaseObj != null && inNextObj != null) {
					inDelta.add(true);
					deltaSerialize(inBB, inBaseObj, inNextObj, inDelta);
				} else {
					inDelta.add(false);
				}
			}
		
		} catch (BufferOverflowException e) {
			throw new NetRunningSerializationException(this, "Failed to delta compress/serialize field.", e);
		}
		
	}
	

	private void deltaSerializeArray (ByteBuffer inBB, Object inBaseArr, Object inNextArr, BitQueue inDelta) throws NetRunningSerializationException {
		log.finest("ENTERED");
		
		
		
		if (inBaseArr == null && inNextArr == null) {			
			inDelta.add(false);
			return;
			
		} else if (inBaseArr != null && inNextArr == null) {
			inDelta.add(true);
			inBB.putInt(-1);	//-1 denotes a null value
			return;
			
		} else if (inBaseArr == null && inNextArr != null) {
			inDelta.add(true);
			
		} else if (inBaseArr != null && inNextArr != null) {
			if (Array.getLength(inBaseArr) != Array.getLength(inNextArr)) {
				inDelta.add(false);
				throw new NetRunningSerializationException(this, "Base array and next array are different lengths - not yet implemented!");
			}
			inDelta.add(true);
			
		} else {
			inDelta.add(false);
			return;
		}
		
		//gets to here, then we are going to delta it
		int len = Array.getLength(inNextArr);
		
		//write the array len to buffer to know how many elements to read when rebuilding
		inBB.putInt(len);
		
		//System.out.println("Serializing array " + inArr.getClass().getSimpleName() + ", len: " + len + " of components " + inArr.getClass().getComponentType());
		for (int i = 0; i < len; i++) {
			try {
				deltaSerializeByType(inBaseArr.getClass().getComponentType().getSimpleName(), inBB, Array.get(inBaseArr, i), Array.get(inNextArr,  i), inDelta);
			} catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
				log.severe("Exception thrown: " + e);
				throw new NetRunningSerializationException(this, "Failed to delta compress/serialize Array");
			}
		}
		
	}
	
	public byte[] deltaSerializeMsg (Message inMsg) throws NetRunningSerializationException {		
	
		
		//get the last delta msg of this time sent
		Message baseMsg = baseDeltaMsgsOut.get(inMsg.getClass());
		
		//if there was none, create a new empty one to delta against
		if (baseMsg == null) {
			try {
				baseMsg = inMsg.getClass().newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new NetRunningSerializationException(this, "Failed to instantiation DeltaMessage class");
			}
		}
		
		//put the new one in as the latest base msg to delta against
		baseDeltaMsgsOut.put(inMsg.getClass(), (DeltaMessage)inMsg);		
		byte[] data = deltaSerialize(baseMsg, inMsg);
		return preappendClassType(data, inMsg.getClass());
	}

	
	private byte[] preappendClassType (byte[] data, Class inClass) throws NetRunningSerializationException {
		
		byte[] returnData = new byte[data.length + 1];
		
		try {
			returnData[0] = classToByteMap.get(inClass);
		} catch (NullPointerException e) {
			throw new NetRunningSerializationException(this, "Class object " + inClass.getSimpleName() + " not registered with the Serializer");
		}
		
		System.arraycopy(data, 0, returnData, 1, data.length);
		return returnData;
		
	}


	
	//fragmented messages don't have the first appended byte to tell how many messages where in the bytearray data
	//we know fragmented messages are only ever a single message though, so can skip that
	public Message deserializeFragmentedMessage (byte[] inBytes) throws NetRunningSerializationException {
		log.finest("ENTERED");
		ByteBuffer BB = ByteBuffer.allocate(inBytes.length + Constants.NET_BYTEBUFFER_UNDERFLOW_SIZE);
		BB.put(inBytes);
		BB.position(0);
		log.info("calling to deserialize fragmented message byte array");
		Message msg = derserializeMsg(BB); 
		return msg;
		
	}
	
	
	public List<Message> deserializeMsgList (ByteBuffer inBB) throws NetRunningSerializationException{
		log.finest("ENTERED");
		
		List<Message> msgs = new LinkedList<Message>();
		
		byte numMsgs = inBB.get();
		log.finer("" + numMsgs + " messages found in messages byte array");
		if (numMsgs == 0) {
			return msgs;
		}
		for (int i = 0; i < numMsgs; i++) {	
			//log.info("attempting to deserialize message " + i + " from messages byte array");
			msgs.add(derserializeMsg(inBB));
		}
		
		return msgs;
		
	}
	
	
	public Message derserializeMsg (ByteBuffer inBB) throws NetRunningSerializationException {
		log.finest("ENTERED");
		byte typeByte = inBB.get();
		Class<? extends Object> msgClass = byteToClassMap.get(typeByte);
		
		if (msgClass == null) {
			throw new NetRunningSerializationException(this, "Message type " + typeByte + " not registered with MessageSerializer");
		}
		Message newMsg;
	
		try {
			newMsg = (Message) msgClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new NetRunningSerializationException(this, "Failed to instantiation Message class");
		}
		
		deserialize(inBB, newMsg);
		
		return newMsg;
	}

	
	public HeaderMsg deserializeHeader (ByteBuffer inBB) throws NetRunningSerializationException {
		HeaderMsg header = new HeaderMsg();
		deserialize(inBB, header);
		header.msgSenderId = header.packetSenderId;
		return header;
	}


	private class ArrayPopulation {
		
		public ArrayPopulation () {
			log.finest("ENTERED");
		}
		
		public void populateBooleanArray (ByteBuffer inBB, Object inArrObj, int inLen) {
			log.finest("ENTERED");
			for (int i = 0; i < inLen; i++) {
				Array.set(inArrObj, i, (inBB.get() == 1? true: false));
			}	
		}
		
		public void populateCharArray (ByteBuffer inBB, Object inArrObj, int inLen) {
			log.finest("ENTERED");
			//System.out.println("arrs::populateCharArray() called..");
			for (int i = 0; i < inLen; i++) {
				Array.set(inArrObj, i, inBB.getChar());
			}	
		}
		
		
		public void populateShortArray (ByteBuffer inBB, Object inArrObj, int inLen) {
			log.finest("ENTERED");
			//System.out.println("arrs::populateShortArray() called..");
			for (int i = 0; i < inLen; i++) {
				Array.set(inArrObj, i, inBB.getShort());
			}	
		}		
		
		public void populateIntArray (ByteBuffer inBB, Object inArrObj, int inLen) {
			log.finest("ENTERED");
			//System.out.println("arrs::populateIntArray() called..");
			for (int i = 0; i < inLen; i++) {
				Array.set(inArrObj, i, inBB.getInt());
			}	
		}
		
		public void populateFloatArray (ByteBuffer inBB, Object inArrObj, int inLen) {
			log.finest("ENTERED");
			//System.out.println("arrs::populateFloatArray() called..");
			for (int i = 0; i < inLen; i++) {
				Array.set(inArrObj, i, inBB.getFloat());
			}	
		}
		
		
		public void populateLongArray (ByteBuffer inBB, Object inArrObj, int inLen) {
			log.finest("ENTERED");
			//System.out.println("ArrayPopulation::populateLongArray() called..");
			for (int i = 0; i < inLen; i++) {
				Array.set(inArrObj, i, inBB.getLong());
			}	
		}
		

		public void populateDoubleArray (ByteBuffer inBB, Object inArrObj, int inLen) {
			log.finest("ENTERED");
			//System.out.println("ArrayPopulation::populateDoubleArray() called..");
			for (int i = 0; i < inLen; i++) {
				Array.set(inArrObj, i, inBB.getDouble());
			}	
			

		}

		public void populateStringArray (ByteBuffer inBB, Object inArrObj, int inLen) {
			log.finest("ENTERED");
			//System.out.println("ArrayPopulation::populateDoubleArray() called..");
			for (int i = 0; i < inLen; i++) {
				
				int strLen = inBB.getInt();
				
				if (strLen == 0) {
					Array.set(inArrObj, i, new String());
				} else {
					byte[] str = new byte[strLen];
					inBB.get(str);
					Array.set(inArrObj, i, new String(str));
				}
				
			}	
		}
		
		
		public void populateObjectArray (ByteBuffer inBB, Object inArrObj, int inLen) throws NetRunningSerializationException {
			log.finest("ENTERED");
			//System.out.println("ArrayPopulation::populateDoubleArray() called..");
			for (int i = 0; i < inLen; i++) {
				
				//0 or 1 is written as a byte at serialization of an object ot 
				if(inBB.get() == 0) {
					continue;
				}
				
				Object arrElement;
				try {
					arrElement = inArrObj.getClass().getComponentType().newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					log.severe("Exception thrown getting array element type from " +  inArrObj.getClass() + ": " +  e);
					throw new NetRunningSerializationException(this, "Failed to instantiate Array object");
				}
				deserialize(inBB, arrElement);
				Array.set(inArrObj, i, arrElement);
			}	
			

		}

				
	}


	
}
