package com.network;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.network.common.Constants;
import com.network.common.Logging;
import com.network.connection.ConnSimulation;
import com.network.connection.ConnectionState;
import com.network.exceptions.NetStartupException;
import com.network.messages.ConnAcceptedMsg;
import com.network.messages.ConnRequestMsg;
import com.network.messages.ConnResponseMsg;
import com.network.messages.PingMsg;
import com.network.messages.PongMsg;
import com.network.packing.Message;
import com.network.profiles.ClientProfile;
import com.network.profiles.NetworkProfile;


/*
 * ClientNetwork
 * Basic in/out layer for user
 * 
 */
public class ClientNetwork extends Network {
	
	private Logger log = Logging.createLogger("ClientNetwork", Level.ALL, Level.INFO);
	
	//this is an array of one profile to work with the Network.getConnectedPorfiles
	//implementation which returns an array of NetworkProfiles and avoid creating an array each call.
	private NetworkProfile serverProfile = new NetworkProfile();
	private ClientProfile localProfile = new ClientProfile();
	
	public ClientNetwork () {
		super(new ConnSimulation(0, 0, 0.5f));
	}

	public boolean isConnected () {
		return localProfile.state == ConnectionState.CONNECTED && isRunning();
	}
	
	

	
	public boolean writeAndFlushMessage (Message inMsg)  {
		writeMessage(inMsg, serverProfile);
		flushMessages();
		return false;
	}

	public void flushMessages () {
		flushMessages(serverProfile, localProfile.profileId);
	}
	
	public void writeMessage (Message inMsg) {
		writeMessage(inMsg, serverProfile);
	}
	
	public NetworkProfile getServerProfile () {
		return serverProfile;
	}
	
	public ClientProfile getLocalProfile () {
		return localProfile;
	}
	
	//exposes a Connect method to the client only
	public void connect (String inAddr, int inPort) throws NetStartupException  {
			
			localProfile.port = ((int)(Math.random() * 3000)) + 3000;
			try {
				localProfile.address = InetAddress.getLocalHost();
			} catch (UnknownHostException e) {
				log.severe("Exception thrown: " + e);
				throw new NetStartupException (this, "Bad local InetAddress: " + e.getMessage());
			}
			
			super.startup(localProfile.port);
			
			try {
				serverProfile.address = InetAddress.getByName(inAddr);
			} catch (UnknownHostException e) {
				log.severe("Exception thrown: " + e);
				throw new NetStartupException (this, "Address failed to convert to InetAddress object: " + e.getMessage());
			}
			serverProfile.port = inPort;
			
			sendConnectionRequest();	//this is a blocking method call that will throw exception if request is not responded to
			sendConnectionAccepted();	//this is also boocking, until a Ping packet is received, this thread is blocked.

		//	super.startNetGraph("Client connection");
	}
	

	
	private void sendConnectionRequest () throws NetStartupException  {
		
		int attempts = 0;	
		//start off on CONNECTING state..
		localProfile.state = ConnectionState.REQUESTING;
		
		//will be set to CONNECTING when connection response packet is read
		while (localProfile.state == ConnectionState.REQUESTING) {	
			log.info("Sending connection request to " + localProfile.address + ":" + localProfile.port + "");
			
			ConnRequestMsg connReq = new ConnRequestMsg();
			connReq.reqFromAddress = this.getLocalProfile().address.getAddress();
			connReq.reqFromPort = this.getLocalProfile().port;
			writeAndFlushMessage(connReq);
			
			//wait a second between tries..
			try {
				Thread.sleep(Constants.CLIENT_CONN_REQ_INTERVAL);
			} catch (InterruptedException e) {
				//expected, do nothing..
			}
			
			//each loop bump the attempts, thrown exception if its too many
			if (attempts++ > Constants.CLIENT_CONN_REQ_ATEMPTS) {
				throw new NetStartupException(this, "Failed to recieve connection response from server " + serverProfile.address + ":" + serverProfile.port);
			}
			if (!isRunning()) {
				throw new NetStartupException(this, "Connection shutdown unexpectedly.");
			}
		} 
	}
	
	public ConnectionState getConnectionState () {
		return localProfile.state;
	}
	
	private void sendConnectionAccepted () throws NetStartupException {
		
		while (this.localProfile.state == ConnectionState.CONNECTING) {
			log.info("Sending connection accepted msg..");
			ConnAcceptedMsg connAcceptMsg = new ConnAcceptedMsg();
			connAcceptMsg.msgSenderId = localProfile.profileId;
			
			writeAndFlushMessage(connAcceptMsg); 
					
			try {
				Thread.sleep(Constants.CLIENT_CONN_ACC_INTERVAL);
			} catch (InterruptedException e) {
				//meh..
			}
			if (!isRunning()) {
				throw new NetStartupException(this, "Connection shutdown unexpectedly.");
			}
		}
		
		log.info("Now connected to server.");

	}
	
	//checks profile is connected and has activity within netProfileTimeoutPreiod range
	protected boolean isTimedOut () {
		return (localProfile.lastActive + Constants.NET_TIMEOUT_PERIOD) < System.currentTimeMillis();
	}	

	protected void disconnect () {
		localProfile.state = ConnectionState.DISCONNECTED;
		shutdown();
	}


	protected void handleConnResMsg (ConnResponseMsg inMsg) {
		if (this.localProfile.state == ConnectionState.CONNECTED) {
			log.fine("Received ConnResponseMsg while localProfile state already CONNECTED..");
			return;
		}
		localProfile.profileId = inMsg.newProfileId;
		this.serverProfile.profileId = inMsg.msgSenderId;
		profiles.put(this.serverProfile.profileId, this.serverProfile);	//needs to be in the profiles to Network level handling of packets/profiles
		localProfile.state = ConnectionState.CONNECTING;
		localProfile.lastActive = System.currentTimeMillis(); //needs setting to avoid timeout check failing before packest are even sent
		
	}

	@Override
	protected boolean handleNetworkMsg (Message inMsg) {
		log.fine("" + inMsg.getClass().getSimpleName() + " received by ClientNetwork..");
		if (inMsg.getClass().equals(ConnResponseMsg.class)) {
			handleConnResMsg((ConnResponseMsg)inMsg);
			return true;
		} else if (inMsg.getClass().equals(PingMsg.class)) {
			handlePingMsg((PingMsg)inMsg);
			return true;
		}
		log.finer("Msg " + inMsg.getClass().getSimpleName() + " not being handled by ClientNetwork..");

		return false;
	}


	
	protected void handlePingMsg (PingMsg inPing) {
		PongMsg pongMsg = new PongMsg();
		pongMsg.timePingCreated = inPing.timeCreated;
		pongMsg.timePongCreated = System.nanoTime();
		this.localProfile.state = ConnectionState.CONNECTED;
		localProfile.lastActive = System.currentTimeMillis(); //needs setting to avoid timeout check failing before packest are even sent
		writeAndFlushMessage(pongMsg);
	}


	@Override
	protected void runNetSpecificProcesses () {
		
		if (isTimedOut()) {
			log.info("Connection to server has timed out.");
			this.disconnect();
		}
		
	}

	
}
