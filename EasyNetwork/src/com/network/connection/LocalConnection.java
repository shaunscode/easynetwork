package com.network.connection;

import java.net.DatagramPacket;

import com.network.exceptions.NetStartupException;

public class LocalConnection extends Connection {
	

	//overriding the run method (the thread method) to do nothing.
	@Override
	public void run () {
		isRunning = true;
	}
	
	//instead of placing outgoing datagrams in the queue for connection loop to pickup and send,
	//we'll just place them from in the in queue for the NetIn to pickup
	@Override
	public void sendDatagram (DatagramPacket inDatagram) {
		synchronized (inDatagramQueue) {
			inDatagramQueue.add(inDatagram);
		}

	}
	

}
