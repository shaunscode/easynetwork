package com.network.connection;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.network.common.Constants;
import com.network.common.Logging;
import com.network.exceptions.NetStartupException;

//Basic network send/receive class
//provides sending datagrams and receiving datagrams
//Network class with processDatagram method exposed is injected into this class
//Network determines how datagram is handled. This class only provides the means 
//of sending and receiving.
public class Connection extends Thread {
	
	private Logger log = Logging.createLogger("Connection", Level.ALL, Level.INFO);
	
	protected boolean isRunning = false;
	protected DatagramSocket socket;
	
	//queues for the incoming and outgoing datagrams.
	//socket reads/writes to them like buffers, allowed Network class to also
	//read/write to them without interrupting the socket
	protected Queue<DatagramPacket> inDatagramQueue = new LinkedList<DatagramPacket>();
	protected Queue<DatagramPacket> outDatagramQueue = new LinkedList<DatagramPacket>();
	
	public Connection () {		

	}

	public void startup (int inPort) throws NetStartupException {
	
		log.finest("ENTERED");
		this.setName("Connection (port " + inPort + ") Thread");

		//create the DatagramSocket..
		try {
			socket = new DatagramSocket (inPort);		
			socket.setSoTimeout(1);
		} catch (SocketException e) {
			throw new NetStartupException(this, e.getMessage());
		}
		
		//start Connection thread loop
		this.start();
		
		//sleep a moment to allow socket to be ready before returning
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
		
				
		
	}
	
	public void run () {
		log.finest("ENTERED");
		
		byte[] data = null;
		DatagramPacket datagram = null;
		isRunning = true;
		
		//run  socket loop inside try/catch/finally block to always close the socket regardless what happens.
		try {
			
			while (isRunning) {	
				
				data = new byte[Constants.NET_DATAGRAM_BUFFER_SIZE];
				datagram = new DatagramPacket(data, data.length);
				
				try {				
					socket.receive(datagram);
					log.finer("Datagram received");
					synchronized (inDatagramQueue) {
						inDatagramQueue.add(datagram);					
					}
	
				} catch (SocketTimeoutException e) {
					//this is expected when socket.setSoTimeout() is used - do nothing
				}
				
				//after timeout OR receiving a packet, it will send all queued datagrams
		//		int sentCount = 0;
				while (outDatagramQueue.size() > 0) {
					synchronized (outDatagramQueue) {	//dont synch unless necessary. even if one is missed, it'll get picked up in 1msec anyway
						try {
							socket.send(outDatagramQueue.poll());
							log.finer("Datagram sent");
						} catch (IOException e) {
							log.warning("Socket failed to send a datagram - Continuing.");
						}
					}
				}
	//			if (sentCount > 0) {
	//				System.out.println("Connection:: " + sentCount + " datagrams sent");
	//			}
			
			}
			log.info("run() Loop has ended (connection.isRunning() returned " + isRunning);
			
		} catch (Exception e) {
			log.severe("" + e.getClass().getSimpleName() + " Exception: " + e.getMessage() + ". Shutting down Connection.");
		} finally {
			isRunning = false;
			socket.close();
		}

	}
	
	public DatagramPacket readDatagram () {
		//log.finest("ENTERED");	//logging this just floods the log
		DatagramPacket datagram = null;
		synchronized (inDatagramQueue) {
			datagram = inDatagramQueue.poll();
		}
		return datagram;
	}
	
	public void sendDatagram (DatagramPacket inDatagram) {
//		log.finest("ENTERED");	//logging this just floods the log
		synchronized (outDatagramQueue) {
			outDatagramQueue.add(inDatagram);
		}

	}
	
	public boolean isRunning () {
		return isRunning;
	}
	
	public boolean stopRunning () {
		isRunning = false;		
		return isRunning;
	}
	
	

}
