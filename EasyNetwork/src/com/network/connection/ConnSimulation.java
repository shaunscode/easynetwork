package com.network.connection;

import java.net.DatagramPacket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.network.common.Logging;

public class ConnSimulation extends Connection {
	
	Logger log = Logging.createLogger("ConnSim", Level.ALL, Level.INFO);
	
	//private Logger log = Logging.getLogger("ConnSimulator");
	private long inLag, inLagVariance, outLag, outLagVariance;
	private float inPL, outPL;
	
	public ConnSimulation () {
		
	}
	
	public ConnSimulation (long inLag, long inLagVar, float inPL) {
		this(inLag, inLagVar, inPL, inLag, inLagVar, inPL);
	}
	
	public ConnSimulation (long inInLag, long inInLagVar, float inInPL, long inOutLag, long inOutLagVar, float inOutPL) {
		inLag = inInLag;
		inLagVariance = inInLagVar;
		inPL = inInPL;
		outLag = inOutLag;
		outLagVariance = inOutLagVar;
		outPL = inOutPL;
		StringBuilder sb = new StringBuilder();
		
		if (inLag > 0 || inLagVariance > 0 || inPL > 0) {
			sb.append("Incoming simulation:\n");
		}
		
		if (inLag > 0) {
			sb.append("\tLag:\t" + inLag + "\n");
		}
		if (inLagVariance > 0) {
			sb.append("\tVariance:\t" + inLagVariance + "\n");
		}
		if (inPL > 0) {
			sb.append("\tPacketLoss:\t" + (inPL * 100)  + "%\n");
		}

		if (outLag > 0 || outLagVariance > 0 || outPL > 0) {
			sb.append("Outgoing simulation:\n");
		}
		
		if (outLag > 0) {
			sb.append("\tLag:\t" + outLag + "\n");
		}
		if (outLagVariance > 0) {
			sb.append("\tVariance:\t" + outLagVariance + "\n");
		}
		if (outPL > 0) {
			sb.append("\tPacketLoss:\t" + (outPL * 100) + "%\n");
		}
		
		log.info("Running Connection simulation:\n" + sb.toString());
		
	}
	

	private Map<Long, DatagramPacket> inLaggedQueue = new HashMap<Long, DatagramPacket>();
	private Map<Long, DatagramPacket> outLaggedQueue = new HashMap<Long, DatagramPacket>();
	
	private Queue<DatagramPacket> inDatagramQueue = new LinkedList<DatagramPacket>();
	private Queue<DatagramPacket> outDatagramQueue = new LinkedList<DatagramPacket>();

	//read the connections datagrams and either drop them or wait before puting them in the inQueue for retrieval next time.
	//if no lag etc they goto the the queue anyway and will be retrieved
	private void readConnectionDatagrams () {
		
		DatagramPacket datagram = null;
		
		StringBuilder sb = new StringBuilder();
		
		while ((datagram = super.readDatagram()) != null) {
			if (sb.length() > 0) {
				log.finest(sb.toString());
				sb.delete(0,  sb.length());
			}
			sb.append("New datagram read..");
			if (getInPL () > 0) {
				sb.append(" PL is at " + (getInPL() * 100) + "%..");
				if (Math.random() < getInPL()) {
					sb.append(" Packet was dropped.");
					log.finest(sb.toString());
					continue;
				}
				sb.append("Packet not dropped..");
			}
			
			//wasn't dropped, check for lag
			if (getInLag() > 0 || getInLagVariance() > 0) {
				long lag = getInLag() + ((long)(getInLagVariance() * Math.random()));
				sb.append("Will be lagged by " + lag + "ms.. ");
				
				inLaggedQueue.put((System.currentTimeMillis()) + lag, datagram);
				
				log.finest(sb.toString());
			} else {
				synchronized (inDatagramQueue) {
					inDatagramQueue.add(datagram);
				}
			}

			log.finest(sb.toString());

			
		}
		//check for lagged packets to re-enter the in queue to be processed
		Entry<Long, DatagramPacket> entry = null;
		Iterator<Entry<Long, DatagramPacket>> iter = inLaggedQueue.entrySet().iterator();
	
	
		while (iter.hasNext()) {
			entry = iter.next();

			if (entry.getKey() < System.currentTimeMillis()) {				
				synchronized (inDatagramQueue) {
					inDatagramQueue.add(entry.getValue());
				}
				iter.remove();
			}
		}
	}
	
	private void sendConnectionDatagram (DatagramPacket inDatagram) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("New datagram being sent..");
		try {
			if (getOutPL() > 0) {
				sb.append(" Outgoing PL at " + (getOutPL() * 100) + "%..");
				if (Math.random() < getOutPL()) {
					sb.append(" Packet was dropped.");
					log.finest(sb.toString());
					return;
				}
			}
			sb.append(" Packet not dropped..");
			//wasn't dropped, check for lag
			if (getOutLag() > 0 || getOutLagVariance() > 0) {
				
				long variance =  ((long)(getOutLagVariance() * Math.random()));
				long lag = getOutLag() + variance;
				sb.append("Will be lagged by " + lag + "ms.. ");
				
				outLaggedQueue.put((System.currentTimeMillis() + lag), inDatagram);
				
				log.finest(sb.toString());
			} else {
				
				//no PL or lag, add to sending queue
				synchronized (outDatagramQueue) {
					outDatagramQueue.add(inDatagram);
				}
			}
			
			
			Entry<Long, DatagramPacket> entry = null;
			Iterator<Entry<Long, DatagramPacket>> iter = outLaggedQueue.entrySet().iterator();
		
			while (iter.hasNext()) {
				entry = iter.next();

				if (entry.getKey() < System.currentTimeMillis()) {				
					synchronized (outDatagramQueue) {
						outDatagramQueue.add(entry.getValue());
					}
					iter.remove();
				}
			}
			
		} catch (NullPointerException e) {
			//log.severe("Exception thrown: " + e);
		}
	}
	
	
	public DatagramPacket readDatagram () {
		readConnectionDatagrams();
		synchronized (inDatagramQueue) {
			return inDatagramQueue.poll();
		}
	}
	
	

	public void sendDatagram (DatagramPacket inDatagram) {
		sendConnectionDatagram(inDatagram);
		while (outDatagramQueue.size() > 0) {
			synchronized (outDatagramQueue) {
				super.sendDatagram(outDatagramQueue.poll());
				log.finest("Datagram has been sent to socket out.");
			}
		}
		
	}

	public long getInLag() {
		return inLag;
	}

	public void setInLag(long inLag) {
		this.inLag = inLag;
	}

	public long getInLagVariance() {
		return inLagVariance;
	}

	public void setInLagVariance(long inLagVariance) {
		this.inLagVariance = inLagVariance;
	}

	public long getOutLag() {
		return outLag;
	}

	public void setOutLag(long outLag) {
		this.outLag = outLag;
	}

	public long getOutLagVariance() {
		return outLagVariance;
	}

	public void setOutLagVariance(long outLagVariance) {
		this.outLagVariance = outLagVariance;
	}

	public float getInPL() {
		return inPL;
	}

	public void setInPL(float inPL) {
		this.inPL = inPL;
	}

	public float getOutPL() {
		return outPL;
	}

	public void setOutPL(float outPL) {
		this.outPL = outPL;
	}
	
	
}
