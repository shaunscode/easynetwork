package com.network.connection;


public enum ConnectionState { 
	REQUESTING, 
	CONNECTING,
	CONNECTED, 
	DISCONNECTED
}
