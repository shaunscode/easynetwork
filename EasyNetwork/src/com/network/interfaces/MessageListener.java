package com.network.interfaces;

import com.network.packing.Message;

public interface MessageListener {
	public void handleMessage (Message inMsg);
}
