package com.network.interfaces;

import java.net.DatagramPacket;

public interface Encryption {

	public DatagramPacket encrypt (DatagramPacket inDatagram);
	public DatagramPacket decrypt (DatagramPacket inDatagram);
	
}
