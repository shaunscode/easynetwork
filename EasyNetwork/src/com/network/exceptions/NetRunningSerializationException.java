package com.network.exceptions;

public class NetRunningSerializationException extends NetRunningException {

	private static final long serialVersionUID = 1L;

	public NetRunningSerializationException () {
		super();
	}

	public NetRunningSerializationException (Object inObj, String inStr) {
		super(inObj, inStr);
	}
	
	public NetRunningSerializationException (Object inObj, String inStr, Exception inEx) {
		super(inObj, inStr, inEx);
	}
}
