package com.network.exceptions;

public class NetStartupException extends NetException {

	private static final long serialVersionUID = 1L;

	public NetStartupException () {
		super();
	}

	public NetStartupException (Object inObj, String inStr) {
		super(inObj, inStr);
	}
	
	public NetStartupException (Object inObj, String inStr, Exception inEx) {
		super(inObj, inStr, inEx);
	}
}
