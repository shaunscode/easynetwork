package com.network.exceptions;

public class NetException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NetException () {
		super();
	}

	public NetException (Object inObj, String inStr) {
		super("" + inObj.getClass().getSimpleName() + ": " + inStr);
	}
	
	public NetException (Object inObj, String inStr, Exception inEx) {
		super("" + inObj.getClass().getSimpleName() + ": " + inEx.getClass().getSimpleName() + ": " + inStr);
	}
}
