package com.network.exceptions;

public class NetShutdownException extends NetException {

	private static final long serialVersionUID = 1L;

	public NetShutdownException () {
		super();
	}

	public NetShutdownException (Object inObj, String inStr) {
		super(inObj, inStr);
	}
	
	public NetShutdownException (Object inObj, String inStr, Exception inEx) {
		super(inObj, inStr, inEx);
	}
}
