package com.network.exceptions;


public class NetRunningRegistrationException extends NetRunningException {
	private static final long serialVersionUID = 1L;
	
	public NetRunningRegistrationException () {
		super();
	}

	public NetRunningRegistrationException (Object inObj, String inStr) {
		super(inObj, inStr);
	}
	
	public NetRunningRegistrationException (Object inObj, String inStr, Exception inEx) {
		super(inObj, inStr, inEx);
	}
}
