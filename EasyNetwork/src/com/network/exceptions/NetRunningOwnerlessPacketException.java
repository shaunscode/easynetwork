package com.network.exceptions;

public class NetRunningOwnerlessPacketException extends NetRunningException {

	private static final long serialVersionUID = 1L;

	public NetRunningOwnerlessPacketException () {
		super();
	}

	public NetRunningOwnerlessPacketException (Object inObj, String inStr) {
		super(inObj, inStr);
	}
	
	public NetRunningOwnerlessPacketException (Object inObj, String inStr, Exception inEx) {
		super(inObj, inStr, inEx);
	}
	
}
