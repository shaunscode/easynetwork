package com.network.exceptions;

public class NetRunningFragmentException extends NetRunningException {

	private static final long serialVersionUID = 1L;

	public NetRunningFragmentException () {
		super();
	}

	public NetRunningFragmentException (Object inObj, String inStr) {
		super(inObj, inStr);
	}
	
	public NetRunningFragmentException (Object inObj, String inStr, Exception inEx) {
		super(inObj, inStr, inEx);
	}
	
}
