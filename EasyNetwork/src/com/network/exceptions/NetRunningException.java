package com.network.exceptions;

public class NetRunningException extends NetException {

	private static final long serialVersionUID = 1L;

	public NetRunningException () {
		super();
	}

	public NetRunningException (Object inObj, String inStr) {
		super(inObj, inStr);
	}
	
	public NetRunningException (Object inObj, String inStr, Exception inEx) {
		super(inObj, inStr, inEx);
	}
}
