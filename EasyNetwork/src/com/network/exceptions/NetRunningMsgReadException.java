package com.network.exceptions;

public class NetRunningMsgReadException  extends NetRunningException {

		private static final long serialVersionUID = 1L;

		public NetRunningMsgReadException () {
			super();
		}

		public NetRunningMsgReadException (Object inObj, String inStr) {
			super(inObj, inStr);
		}
		
		public NetRunningMsgReadException (Object inObj, String inStr, Exception inEx) {
			super(inObj, inStr, inEx);
		}
		
	}
