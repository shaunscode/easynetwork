package com.network.exceptions;

public class NetStartupConnectionFailedException extends NetException {

	private static final long serialVersionUID = 1L;

	public NetStartupConnectionFailedException () {
		super();
	}

	public NetStartupConnectionFailedException (Object inObj, String inStr) {
		super(inObj, inStr);
	}
	
	public NetStartupConnectionFailedException (Object inObj, String inStr, Exception inEx) {
		super(inObj, inStr, inEx);
	}
}
