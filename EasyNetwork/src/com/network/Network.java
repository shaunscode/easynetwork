package com.network;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.network.common.Constants;
import com.network.common.Logging;
import com.network.connection.Connection;
import com.network.exceptions.NetRunningFragmentException;
import com.network.exceptions.NetRunningSerializationException;
import com.network.exceptions.NetStartupException;
import com.network.messages.DisconnectMsg;
import com.network.messages.HeaderMsg;
import com.network.packing.Message;
import com.network.packing.MessageData;
import com.network.profiles.NetworkProfile;
import com.network.serialization.Serializer;
import com.network.utils.NetGraph;
import com.network.utils.NetUtils;


public abstract class Network {
	
	private Logger log = Logging.createLogger("Network", Level.ALL, Level.FINE);


	protected NetworkProfile localProfile = new NetworkProfile();
	
	protected HashMap<Integer, NetworkProfile> profiles = new HashMap<Integer, NetworkProfile>();
	
	private NetGraph netGraph = null;
	
	private Queue<Message> msgsInQueue = new LinkedList<Message>();
	
	private Connection connection;
	private Staging stage;
	
	public Network () {
		connection = new Connection();
		stage = new Staging(connection);
	}
	
	public Network (Connection inConn) {
		connection = inConn;
		stage = new Staging(connection);
		
	}
	
	public void setConnection (Connection inConn) {
		this.connection = inConn;
		
		log.fine("New Connection obj (" + inConn.getClass().getSimpleName() + ") has been set in " + this.getClass().getSimpleName() + " obj");
	}
	
	protected void startup (int inPort) throws NetStartupException  {
		log.finest("ENTERED");
		if (inPort == -1) {
			throw new NetStartupException(this, "Bad port number: " + inPort);
		}

		log.info("Starting connection..");
		connection.startup(inPort);
		//Connection constructor is thead blocking for approx 500ms while Connection thread starts up.
	
		log.info("Connection and Staging ready.");
		
		
		runProcessingLoop();
	}
		
	protected void shutdown () {
		for (int i = 0; i < 5; i++) {
			for (NetworkProfile profile: profiles.values()) {
				//sendPacket(new Disconnect(), profile);
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				//shutting down anyways..
			}
		}
		connection.stopRunning();
		
	}

	
	public void startNetGraph (final String inTitle) {
		
		new Thread (new Runnable () {
			@Override
			public void run() {
				//System.out.println("### Creating new net graph");
				netGraph = new NetGraph(inTitle, profiles);
			}			
		}).start();
		
		
	}

	public boolean hasMessages () {
		if (msgsInQueue.size() > 0) {
			return true;
		}
		return false;
	}
	
	//this is used to avoid new objs being created every readMessages() call
	//perhaps a double buffered msgsInQueue which is returned couold remove the time spent clearing them
	private List<Message> returningMsgs = new LinkedList<Message>();
	
	public List<Message> readMessages () {
		returningMsgs.clear();
		 synchronized (msgsInQueue) {
			 returningMsgs.addAll(msgsInQueue);
			 msgsInQueue.clear();
		 }
		 return returningMsgs; 
	}
	
	
	private void runStageProcesses () throws NetRunningSerializationException, NetRunningFragmentException {
		
		long currentTime = System.currentTimeMillis();
		//parse and new msgs
		
		for (Message msg: stage.getMessages()) {

			NetworkProfile profile = this.getProfileById(msg.msgSenderId);
			if (profile != null) {
				profile.lastActive = currentTime;
			}

			if (msg.getClass().equals(HeaderMsg.class)) {
					handleHeaderMsg((HeaderMsg)msg);
			} else if (handleNetworkMsg(msg)) {	//Implemented in Client/Server, handle Client/Server messages
			} else {
				 synchronized (msgsInQueue) {
					 msgsInQueue.add(msg);		//must be a user msg otherwise, put it in their queue for handling
				 }
			}
			
		}
		
	}

	
	public void writeMessage (Message inMsg, NetworkProfile inProfile) {
		stage.writeMsgToProfile(inMsg, inProfile);
	}
	
	public void writeReliableMessage (Message inMsg, NetworkProfile inProfile) {
		inMsg.msgReliable = true;
		stage.writeMsgToProfile(inMsg, inProfile);
	}
	
	
	
	public void flushMessages (NetworkProfile inProfile, int inFromProfileId) {
		stage.flushMessages(inProfile, inFromProfileId);
	}
	
	public void registerClass (Class<? extends Object> inClass) {
		stage.registerClass(inClass);
	}
	
	public void registerIgnoredClassField (String inFieldName) {
		Serializer.addIgnoredFieldName(inFieldName);
	}
	
	//for overriding by client/server classes
	protected abstract boolean handleNetworkMsg (Message inMsg);
	
	private void handleHeaderMsg (HeaderMsg inMsg) {

		NetworkProfile profile = this.getProfileById(inMsg.msgSenderId);
		if (profile == null) {
			return;
		}
		int index = NetUtils.getBufferIndex(inMsg.packetId);
		profile.inPacketsRegister[index] = true;
		//System.out.println("handleHeaderMsg: packetId registered: " + inMsg.packetId + " @ " + index);
		
		//don't want to lower the ack from a late arriving packet or bother with old ack bits 
		if (inMsg.packetId < profile.packetInId) {	
			return;
		}
		
		int lastIndex = NetUtils.getBufferIndex(profile.packetInId);
		index = NetUtils.prevBufferIndex(index);
		
		
		while (index != lastIndex) {
			profile.inPacketsRegister[index] = false;
			profile.outPacketsBuffer[index].wasReceived = false;
			index = NetUtils.prevBufferIndex(index);
		}
		
		
		profile.packetInId = inMsg.packetId;	
	
		processAckBits(inMsg.ackId, inMsg.ackBits, profile);
		
		
	}
	
	//go over the MsgData's of the packet being resent and only resend reliable msgs
	private void resendReliableMsgs (List<MessageData> inMsgs, NetworkProfile inProfile) { 
					
		for (MessageData msgData: inMsgs) {
			if (msgData.reliable) {
				synchronized (inProfile.messageDatasOut) {		
					inProfile.messageDatasOut.add(0, msgData);	//add the msg to the start of the msg datas to be sent	
				}
			}
		}
	}
	
	
	//goes over connectioned profiles and checks their last activity
	protected abstract void runNetSpecificProcesses ();



	protected boolean isRunning () {
		return connection.isRunning();
	}
	
	

	/*	go over this profiles ack bits
	anything that hasn't been receieved, check if it was a reliable and resend
	#### IMPORTANT ####
	inPacket.ackId IS NOT THE SAME AS profile.ackId!
	inPacket.ackId is telling us what packets the sender has last received - THIS person needs to resend the missing packets relative to what THEY have recieved
	profile.ackId is holding the packet the profile last received. - THIS person will send this to other end to receive the packets they didn't receive yet have
*/	
	private void processAckBits (int inAckId, int inAckBits, NetworkProfile inProfile) {

		
			
		//the +1 here to get the index - not sure why the alignment of bits relative to the ackId is one off
		//when getting the index of packet to resend..., but the +1 fixes it. TODO: fix this? if it ain't broke...
		int index = NetUtils.getBufferIndex(inAckId - Constants.PROFILE_PACKET_ACK_BITS_LENGTH + 1);
		index = NetUtils.clampBufferIndex(index);
		//starting index is the first bit referred to -> last bit packed into ackBits -> "Constants.networkAckBitSize" packets before current packet ack 
		//using index as the accessing position, loop "Constants.networkAckBitSize" times to read all packed bits
		
		
		//System.out.print("processAckBits::\tackId: " + inAckId + ",\tindex: " + (NetUtils.getBufferIndex(inAckId)) + ",\t index of first bit to read: " + index + ",\tackBits >>> IN\t: " +
		//NetUtils.getBinary(inAckBits));
				
		
		for (int i = 0; i < Constants.PROFILE_PACKET_ACK_BITS_LENGTH; i++) {
			

			if ((inAckBits & 1) != 1) {
				if (inProfile.outPacketsBuffer[index].resent == false) {	
					inProfile.outPacketsBuffer[index].wasReceived = false;
					if (inProfile.outPacketsBuffer[index].hasReliableMessages) {
						resendReliableMsgs(inProfile.outPacketsBuffer[index].msgs, inProfile);
						inProfile.outPacketsBuffer[index].resent = true;
					}
				}
			} else {
				inProfile.outPacketsBuffer[index].wasReceived = true;
			}
		
			//move the bits back down the line
			inAckBits = inAckBits >>> 1;
			index = NetUtils.nextBufferIndex(index);			
			
			
		}
		
	//	index = NetUtils.prevBufferIndex(index);
		//System.out.print(",\t last index read: " + index + "\n");
		
	}
	
	
	/*		
	private void sendProfileOutgoingMessages (NetworkProfile inProfile) {		
		 while (inProfile.messageOutList.size() > 0) {	
			 log.finer("sending profile messages to " + inProfile.profileId);
			 packMessagesAndSend(inProfile);	
		}
	}*/
	

	


	

	

	
	protected NetworkProfile getProfileByAddress (InetAddress inAddr, int inPort) {
		if (inAddr == null || inPort == -1) {
			log.warning("Bad address while finding connected client: " + inAddr + ":" + inPort);
			return null;
		}
		for (NetworkProfile profile: profiles.values()) {
			if (profile.address.equals(inAddr) && profile.port == inPort) {
				return profile;
			}
		}
		return null;
	}
	

	
	public NetworkProfile getProfileById (int inProfileId) {
		return profiles.get(inProfileId);
	}
	
	public Collection<NetworkProfile> getAllProfiles() {
		return profiles.values();
	}
	
	private void runProcessingLoop () {
		

		log.finest("ENTERED");
		
		Thread processingThread = new Thread (new Runnable () {
			
			public void run () {
				//NetOutput.info(this, this, "runProcessingLoop(): started..");
				
				try {
					
					log.info("Starting Network processing loop..");
					
					while (connection.isRunning()) {
						
						//pull msgs from stage and all the accompanying processes
						runStageProcesses(); 
						
						//run client/server processes
						runNetSpecificProcesses();	
						
						//sleep the thread for a moment as per tick rate
						try {                                                                                         
							Thread.sleep(Constants.NET_PROCESSING_INTERVAL);
						} catch (InterruptedException e) {
							//expected
						}
						
												
						if (netGraph != null) {
							netGraph.repaint();
						}
						
					}				
					log.info("Loop has ended (connection.isRunning() returned " + connection.isRunning());
					
				} catch (Exception e) {
					log.severe("Exception throw: " + e);
				} finally {
					connection.stopRunning();
				}
			}
			
		});
		processingThread.setName("Network::Processing Thread");
		processingThread.start();
	}
	
	protected void disconnectProfile (NetworkProfile inProfile) {
		DisconnectMsg disconnect = new DisconnectMsg();
		//connectingProfiles.remove(inProfile.profileId);
		profiles.remove(inProfile);
		//sendPacket(disconnect, inProfile);
	}

	
	


	
	
}
