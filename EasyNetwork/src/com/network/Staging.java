package com.network;

import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.network.common.Constants;
import com.network.common.FragmentSet;
import com.network.common.Logging;
import com.network.connection.Connection;
import com.network.exceptions.NetRunningFragmentException;
import com.network.exceptions.NetRunningSerializationException;
import com.network.messages.HeaderMsg;
import com.network.packing.Fragment;
import com.network.packing.Message;
import com.network.packing.MessageData;
import com.network.packing.PacketData;
import com.network.profiles.NetworkProfile;
import com.network.serialization.Serializer;
import com.network.utils.NetUtils;

public class Staging {

	private final Serializer serializer = Serializer.getInstance();
	private final Connection connection;

	private Logger log = Logging.createLogger("Staging", Level.ALL, Level.FINE);

	private static byte fragmentIdCounter = 0;

	private HashMap<Byte, FragmentSet> fragments = new HashMap<Byte, FragmentSet>();

	public Staging(Connection inConnection) {
		this.connection = inConnection;
	}

	public void flushMessages(NetworkProfile inProfile, int inFromProfileId) {

		if (inProfile.messageDatasOut.size() > 0) {
	
			ByteBuffer bb = ByteBuffer.allocate(Constants.NET_DATAGRAM_BUFFER_SIZE);

			PacketData packetData = new PacketData();

			HeaderMsg header = getNewPacketHeader(inProfile, inFromProfileId);

			try {
				packHeader(bb, packetData, header);
				synchronized (inProfile.messageDatasOut) {	//sync'ed, as user app in separate thread can call to flush/retrieve msgs
					packMessages(bb, inProfile.messageDatasOut, packetData);
				}
			} catch (NetRunningSerializationException e) {
				log.warning("Failed to flush msgs to profile: " + e.getMessage());
			}

			DatagramPacket datagram = new DatagramPacket(bb.array(), bb.capacity());
			datagram.setAddress(packetData.header.sendingToAddress);
			datagram.setPort(packetData.header.sendingToPort);
			connection.sendDatagram(datagram);
			// store them anyway, their id should get pcked up as dropped and reliable msgs
			// will be resent
			int index = NetUtils.getBufferIndex(packetData.header.packetId);
			inProfile.outPacketsBuffer[index] = packetData;

		}
	}

	private HeaderMsg getNewPacketHeader(NetworkProfile inProfile, int inFromProfileId) {
		HeaderMsg header = new HeaderMsg();
		copyProfileDataToPacket(header, inProfile);
		packAckBits(header, inProfile.inPacketsRegister);
		header.packetSenderId = inFromProfileId;
		log.finer("built header, assigned sender id: " + inFromProfileId);
		return header;
	}

	private void packHeader(ByteBuffer inBB, PacketData inPacket, HeaderMsg inHeader)
			throws NetRunningSerializationException {
		inBB.put(serializer.serialize(inHeader));
		inPacket.header = inHeader;
	}

	// goes over the profiles receivedPacketAcks array and pushings bits onto
	// the ackBits variable to send across the network.
	// Receiver or packet should the go over it and send anything not received that
	// was reliable
	private void packAckBits(HeaderMsg inHeader, boolean[] inInPacketsReg) {

		// get index of packet ID
		int index = NetUtils.getBufferIndex(inHeader.ackId);
		//System.out.print("packAckBits:\tackId (last packet received): " + inHeader.ackId + " @ "+ index + "");
		
		
		// reset ack bits
		inHeader.ackBits = 0;

		for (int i = 0; i < Constants.PROFILE_PACKET_ACK_BITS_LENGTH; i++) {

			// this is up first, as if last, it will push the bits up after the last pack
			// and leave a 0 bit at the tail end
			inHeader.ackBits = inHeader.ackBits << 1;

			if (inInPacketsReg[index]) {
				inHeader.ackBits++;
			}

			index = NetUtils.prevBufferIndex(index);
		}

		//index = NetUtils.nextBufferIndex(index);
		//System.out.print(", Last reg index packed: " + index);
		//System.out.println("\t ackBits: " + NetUtils.getBinary(inHeader.ackBits));
	}

	private void copyProfileDataToPacket(HeaderMsg inHeader, NetworkProfile inProfile) {
		inHeader.packetId = ++inProfile.packetOutId;
		inHeader.sendingToAddress = inProfile.address;
		inHeader.sendingToPort = inProfile.port;
		inHeader.ackId = inProfile.packetInId;
		inHeader.connectedAckId = inProfile.packetInId;
	}

	private void packMessages(ByteBuffer inBB, List<MessageData> inMsgs, PacketData inPacketData)
			throws NetRunningSerializationException {

		// use a byte to count so we can slip it into the buffer array.Couldn't send
		// more than 127 different msgs in one packet, right? :S
		byte numMsgs = 0;
		int numMsgsIndex = inBB.position(); // get current index (after header has been written) where numMsgs value
											// will be place after msgs are packed. -1 as position() 5 is really index
											// 4.
		inBB.position(numMsgsIndex + 1); // move the writing index up one

		ListIterator<MessageData> iter = inMsgs.listIterator();

		while (iter.hasNext()) {

			MessageData msgData = iter.next();

			// if this single msg length is greater that the dataSize, which wouold max at
			// at Constants.net_max_datagram_size, it
			// is a msg that is too large for a normal packet and will have to be fragmented
			// remove it and send it as it's own
			if (msgData.data.length > inBB.remaining()) { // msg is larger then the remianing space?

				if (msgData.fragment) {
					// already a fragment? do nothing
				} else if (msgData.data.length < Constants.NET_FRAGMENT_SIZE) {
					// not a fragment but smaller than a fragment would be? why fragment it then?
				} else {
					// iter.remove() needs to be called first as adding the fragments to iter
					// (further down) puts the fragments BEFORE this iter's index
					// but we want to process them now, so we iter.previous() after each iter.add().
					// If we were to iter.remove()
					// afterwards, we'd be a number of indices away from the element we actually
					// want removed
					iter.remove();

					// to get here must not be a fragment and is large enough to break into
					// fragments
					List<MessageData> fragmentDatas = fragmentLargeMsg(msgData);
					for (MessageData fragmentData : fragmentDatas) {
						iter.add(fragmentData);
						iter.previous(); // returns iterator to the just added fragments index
					}
				}
			} else {
				// else remove and pack it
				iter.remove();
				inBB.put(msgData.data);
				inPacketData.msgs.add(msgData);
				if (msgData.reliable) {
					//System.out.println("packet hasReliableMsgs has been set to true!");
					inPacketData.hasReliableMessages = true;
				}
				numMsgs++; // add to num msgs in packet
				

			}

		}

		// after that put hte num of msgs that were packed back at the beginning of the
		// packed msgs
		inBB.put(numMsgsIndex, numMsgs);

	}

	// we'll make fragments a quart the size of the max datagram buffer size so that
	// it's easier to fit the
	// fragment message into packets. a single one may squeeze in the end of the
	// current packet being sent
	// of two might fit in, or three..
	private Fragment getFragment(ByteBuffer bb) {

		Fragment fragment = new Fragment();
		int fragLen = Constants.NET_FRAGMENT_SIZE;

		if (bb.position() + fragLen > bb.limit()) {
			fragLen = bb.limit() - bb.position();
		}

		fragment.data = new byte[fragLen];
		bb.get(fragment.data, 0, fragLen);

		return fragment;
	}

	private List<Fragment> getFragments(byte[] inMsgByteData) {
		List<Fragment> fragments = new LinkedList<Fragment>();

		ByteBuffer bb = ByteBuffer.wrap(inMsgByteData);

		while (bb.remaining() > 0) {
			fragments.add(getFragment(bb));
		}
		return fragments;
	}

	private MessageData getFragmentData(Fragment inFragment) throws NetRunningSerializationException {
		MessageData fragData = getMsgData(inFragment);
		fragData.fragment = true;
		return fragData;
	}

	private List<MessageData> getFragmentDatas(List<Fragment> inFragments) throws NetRunningSerializationException {
		List<MessageData> fragDatas = new LinkedList<MessageData>();
		for (Fragment fragment : inFragments) {
			fragDatas.add(getFragmentData(fragment));
		}
		return fragDatas;
	}

	private void addFragmentInfo(boolean isReliableMsg, List<Fragment> inFragments) {

		byte fragmentId = fragmentIdCounter++;
		int index = 0;
		int total = inFragments.size();

		for (Fragment frag : inFragments) {
			frag.fragmentId = fragmentId;
			frag.index = index++;
			frag.totalFragments = total;
			frag.msgReliable = isReliableMsg;
		}

	}

	// this MessageData has been removed from the outgoing messages and will be
	// dereferenced after this method
	private List<MessageData> fragmentLargeMsg(MessageData inMsgData) throws NetRunningSerializationException {
		log.finest("ENTERED");
		List<Fragment> fragments = getFragments(inMsgData.data);
		addFragmentInfo(inMsgData.reliable, fragments);
		List<MessageData> fragmentDatas = getFragmentDatas(fragments);
		return fragmentDatas;
	}
	
	private MessageData getMsgData(Message inMsg) throws NetRunningSerializationException {
		MessageData data = new MessageData();
		data.data = serializer.serialize(inMsg);
		data.reliable = inMsg.msgReliable;
		data.msg = inMsg;
		return data;
	}

	List<Message> allNewMsgs = new LinkedList<Message>();

	// interface method for Staging called by NetIn obj
	public List<Message> getMessages() throws NetRunningSerializationException, NetRunningFragmentException {

		// gotta clear this from any old msgs that still may be there.
		allNewMsgs.clear();

		DatagramPacket datagram = null;

		// get a datagram
		while ((datagram = connection.readDatagram()) != null) {

			// deserialise it to header/messages
			List<Message> senderNewMsgs = getDatagramMsgs(datagram);

			// remove Fragment msgs from the msgs that just came in. adds them to
			// fragmentSets
			pullNewFragments(senderNewMsgs);

			// put all new msg from this datagram into the returning msgs list
			allNewMsgs.addAll(senderNewMsgs);
		}

		// check the fragment sets for any completed ones we can turn into msgs again
		rebuildFragmentedMsgs(allNewMsgs);

		// return the accumulation of all msgs that have come in
		return allNewMsgs;
	}

	// processes raw datagram data byte[] array
	// turns into messages, stores fragments, creates updateProfileMsg
	private List<Message> getDatagramMsgs(DatagramPacket inDatagram) throws NetRunningSerializationException {

		ByteBuffer bb = ByteBuffer.wrap(inDatagram.getData());

		// first pull header
		HeaderMsg header = serializer.deserializeHeader(bb);

		// then the messages
		List<Message> newMsgs = serializer.deserializeMsgList(bb);

		// update all the msgs with the header details
		for (Message msg : newMsgs) {
			msg.msgSenderId = header.packetSenderId;
			msg.msgPacketId = header.packetId;
		}

		// add the header to the new msgs also for later use
		newMsgs.add(header);

		// return them all..
		return newMsgs;

	}

	// iterate over fragmentsets and add and completed sets to the msg list in
	private void rebuildFragmentedMsgs(List<Message> inRetMsgs) {

		if (fragments.size() > 0) {

			// get an iterator over the entries so we can remove them cleanly from the map
			Iterator<Entry<Byte, FragmentSet>> iter = fragments.entrySet().iterator();

			// local entry to store iter element
			Entry<Byte, FragmentSet> entry = null;

			while (iter.hasNext()) {
				entry = iter.next();

				// if its a complete fragment set, process it.
				if (entry.getValue().isComplete()) {
					
					try {
						Message msg = serializer.deserializeFragmentedMessage(entry.getValue().getData());
						msg.msgSenderId = entry.getValue().getSenderId();
						inRetMsgs.add(msg);
					} catch (NetRunningSerializationException | NetRunningFragmentException e) {
						log.warning("Failed to convert completed fragment to new message from byte data, id " + entry.getKey());
					}
					iter.remove();

				}
			}
		}
	}


	private void pullNewFragments(List<Message> inMsgs) {
		Iterator<Message> iter = inMsgs.iterator();
		Message msg = null;
		while (iter.hasNext()) {
			msg = iter.next();
			if (msg.getClass().equals(Fragment.class)) {
				addFragment((Fragment) msg);
				iter.remove();
			}
		}
	}

	public void addFragment(Fragment inFragment) {
		log.fine("Fragment received: Id: " + inFragment.fragmentId + ", data.len: " + inFragment.data.length
				+ ", fragment: " + (inFragment.index + 1) + " of " + inFragment.totalFragments);

		// try find a fragset that matches this fragments id
		FragmentSet fragSet = fragments.get(inFragment.fragmentId);

		// if no fragset was found, make one and put it in the map
		if (fragSet == null) {
			log.finer("created FragmentSet for fragment Id " + inFragment.fragmentId);
			fragSet = new FragmentSet();
			fragments.put(inFragment.fragmentId, fragSet); // put this fragset in the profile sets against it's fragment
															// id
		}

		// try add fragment to set
		try {
			log.finer("adding Fragment packet to FragmetnSet (Id: " + inFragment.fragmentId + ")");
			fragSet.addFragment(inFragment);
		} catch (NetRunningFragmentException e) {
			log.warning("Exception while adding a fragment " + ((Fragment) inFragment).fragmentId + " to the fragment: "
					+ e.getMessage());
		}
	}

	public void registerClass(Class<? extends Object> inClass) {
		serializer.registerClass(inClass);
	}

	public void writeMsgToProfile(Message inMsg, NetworkProfile inProfile) {
		try {
			MessageData msgData = null;
			msgData = getMsgData(inMsg);
			synchronized (inProfile.messageDatasOut) {
				inProfile.messageDatasOut.add(msgData);
			}
		} catch (NetRunningSerializationException e) {
			log.warning("Failed to serialise " + inMsg.getClass().getSimpleName() + " msg to msgData - discarding");
		}
	}

}
