package com.network.profiles;

import com.network.common.Constants;
import com.network.connection.ConnectionState;

public class ClientProfile  extends NetworkProfile {


	public ConnectionState state = ConnectionState.DISCONNECTED;
	
	public long lastPinged = System.currentTimeMillis();		//used by ServerNetwork to determine when to send a ping msg: using lastActive + Constants.Ping_interval would send bursts of pings (one per Network.runProcesses()->runNetSpeicifcProcesses() call, which is 1millisec?) to clients with high PL that didn't respond or get a Pong through. 

	public long[] pings = new long[Constants.PROFILE_RTT_PINGS_BUFFER_LENGTH];	
	public float RTT = 0;
	
	
	public String toString () { 
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append(" ConnState: " + state.name());
		return sb.toString();
	}
	
}
