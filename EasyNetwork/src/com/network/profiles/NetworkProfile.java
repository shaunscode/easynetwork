package com.network.profiles;

import java.net.InetAddress;
import java.util.LinkedList;
import java.util.List;

import com.network.common.Constants;
import com.network.packing.Message;
import com.network.packing.MessageData;
import com.network.packing.PacketData;

public class NetworkProfile {
	
	public InetAddress address = null;
	public int port = -1;
	public int profileId = 0;
	
	public int packetInId = 0;	//aka the AckId to build AckBits relative to when sending a packet out
	public int packetOutId = 1;	//need to start at 1, otherwise the write index will be 0 on the first msg sent, and first processing will made read equal 0, and will not send any messages until a second msg is sent 

	public long lastActive = System.currentTimeMillis();		//time last packet was received - use for checking send rate/time. NOTE: Starts as current time, as it's not set anywhere else until a packet is sent, which only happens when this + rate > current time
	
	
	//public long rate = 50;	//msecs between sending data to this client, 50msecs = 20fps
	//public int packetDataSize = Constants.NET_MAX_DATAGRAM_SIZE;
	
	//public int msgBufferDataSize = 0;	//whenever a msg is added, the size of the msgData is added
													//whenever one is read, the size of that msgData is removed
	
		
	//public List<Message> newMsgsOut = new LinkedList<Message>();	//raw messages to be sent, stored pre-pushing to stage
	public List<MessageData> messageDatasOut = new LinkedList<MessageData>();	//serialized messages/data, to be sent at stage layer
	
	public PacketData[] outPacketsBuffer = new PacketData[Constants.PROFILE_SENT_PACKETS_BUFFER_LENGTH];	//post-sent storage of sent msgs for resent purposes
	
	public boolean[] inPacketsRegister = new boolean[Constants.PROFILE_SENT_PACKETS_BUFFER_LENGTH]; //why is this used?
	
	public NetworkProfile () {
		for (int i = 0; i < inPacketsRegister.length; i++) {
			inPacketsRegister[i] = true;
		}	
		for (int i = 0; i < outPacketsBuffer.length; i++) {
			outPacketsBuffer[i] = new PacketData();
			outPacketsBuffer[i].wasReceived = true;
		}
	}	
	
	
	public String toString () {
		return "ProfileId: " + profileId + " (" + address + ":" + port + ")";
	}
}
