package com.network.packing;

import java.net.InetAddress;

import com.network.common.Constants;

public class PacketHeader {
	
	public InetAddress sendingToAddress = null;	//ignored field
	public int sendingToPort = -1;		//ignored field 
	
	public int packetSenderId = -1;	//Message objs have 'senderId' which is ignored on sending, as we don't want to send it n * msgs amount of times. So send this once, assign it to all msgs on other end.
	
	public int packetId = -1; 	//4 bytes
	public byte version = Constants.VERSION;		//1 byte
	
	public int ackId = 0; 	//4 bytes
	public int ackBits = 0; 	//4 bytes
	public int connectedAckId = 0; 	//4 bytes
	
}
