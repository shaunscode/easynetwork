package com.network.packing;

import java.util.LinkedList;
import java.util.List;

import com.network.messages.HeaderMsg;

public class PacketData {

	//needed?
	public long timeCreated;
	
	//used in bacpket buffer
	public	boolean hasReliableMessages = false;
	public	boolean resent = false;
	public boolean wasReceived = false;
	
	//details of what was sent
	public HeaderMsg header;
	public List<MessageData> msgs = new LinkedList<MessageData>();
	
}
