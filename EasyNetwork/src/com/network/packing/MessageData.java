package com.network.packing;

public class MessageData {
	
	public boolean fragment = false;
	public boolean reliable = false;
	public Message msg = null;
	public byte[] data = null;

}
